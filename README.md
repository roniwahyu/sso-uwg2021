# sso-uwg
**Single Sign On - Universitas Widyagama**

Teknologi *Sigle Sign On* Universitas Widyagama merupakan teknologi login terpusat dimana sistem - sistem yang berbeda dalam satu Universitas dapat terintegrasi dengan satu user Account yang valid. 


Dikembangkan oleh :
-------------------
1. Dr. Istiadi, ST., MT
2. Ismail Akbar, S.Kom
3. Kuncahyo Setyo Nugroho, S.Kom

Dibawah :
---------
Pusat IT & PDPT Universitas Widyagama

Copyright © 2019
