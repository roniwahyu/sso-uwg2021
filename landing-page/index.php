<?php
session_start();
  include('../koneksi/koneksi.php');
  // include('../../simawa/page/mhs/login_session.php');
  date_default_timezone_set("Asia/Jakarta");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Metro 4 -->
    <link href="assets/metro-all.css?ver=@@b-version" rel="stylesheet">
    <link href="assets/start.css" rel="stylesheet">
    <link href="assets/modal.css" rel="stylesheet">
    <link href="assets/bootstrap.css" rel="stylesheet">
    <link href="assets/bootstrap.min.css" rel="stylesheet">

    <title>SSO | Universitas Widyagama</title>
    <link href="assets/images/favicon.png" rel="shortcut icon">
  </head>

  <body>
    <div class="container-full-bg">
      <!--jumbotron-->
      <div class="jumbotron jumbotron-fluid text-center" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);">
        <div class="container text-white">
          <h2 class="card-title h2">Single Sign-On | Universitas Widyagama</h2>
          <p class="card-text">Teknologi <em>Single Sign-On</em> (SSO) Universitas Widyagama merupakan teknologi login terpusat dimana sistem yang berbeda dalam satu Universitas dapat terintegrasi dengan satu user <em>account</em> yang valid. </p>
          <a class="btn btn-lg button-custom btn-block" href="https://sso.widyagama.ac.id/spada/login/signin.php" >MONEV E-LEARNING DOSEN</a>
          <a class="btn btn-lg button-custom" href="https://sso.widyagama.ac.id/spada/kelas.php" >INFO KELAS</a>
        </div>
      </div>

      <div class="container start-custom">
        
      </div>

      <div class="footer">
      </br>
      <p style="text-align: center;">Copyright &copy; 2019-2020. Powered by <strong>Pusat IT & PDPT Universitas Widyagama</strong></p>
    </div>
      <!-- jQuery first, then Metro UI JS -->
      <script src="assets/jquery-3.3.1.min.js"></script>
      <script src="assets/bootstrap.min.js"></script>
      <script src="assets/bootstrap.js"></script>
      <script src="assets/metro.min.js"></script>
      <script src="assets/start.js"></script>

  </body>
</html>

<!-- Start Modal SIMAWA -->
<div id="simawa" class="modal fade" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-simawa">
        <h5 class="modal-title">Sistem Informasi Mahasiswa</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIMAWA.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIMAWA</strong></h5>
            <p class="pmodal"> Sistem informasi bagi mahasiswa dalam mengkases informasi selama proses perkuliahan seperti biodata, nilai, jadwal kuliah, dan lainnya.</p>
            <p class="pmodal"><strong>Pengguna : Mahasiswa</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal" data-toggle="modal" data-target="#develop">Login Sistem</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIMAWA -->

<!-- Start Modal SIMPEG -->
<div id="simpeg" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-simpeg">
        <h5 class="modal-title">Sistem Informasi Kepegawaian</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIMPEG.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIMPEG</strong></h5>
            <p class="pmodal"> Sistem informasi bagi mahasiswa dalam mengkases informasi selama proses perkuliahan seperti biodata, nilai, jadwal kuliah, dan lainnya.</p>
            <p class="pmodal"><strong>Pengguna : BAUK, Admin Kepegawaian</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <a href="../../simpeg/login/signin.php"><button type="submit" class="btn btn-outline-primary">Login Sistem
        </button></a>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIMPEG -->

<!-- Start Modal SIWISU -->
<div id="siwisu" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-siwisu">
        <h5 class="modal-title">Sistem Informasi Wisuda</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIWISU.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIWISU</strong></h5>
            <p class="pmodal"> Sistem informasi layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : Mahasiswa, Panitia Wisuda</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal" data-toggle="modal" data-target="#develop">Login Sistem</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIWISU -->

<!-- Start Modal SIMAS -->
<div id="simas" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-simas">
        <h5 class="modal-title">Sistem Informasi Assets</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIMAS.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIMAS</strong></h5>
            <p class="pmodal"> Sistem informasi layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : BAUK</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal" data-toggle="modal" data-target="#develop">Login Sistem</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIMAS -->

<!-- Start Modal SIPAKAR -->
<div id="sipakar" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-sipakar">
        <h5 class="modal-title">Sistem Informasi Pengembangan Karir</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIPAKAR.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIPAKAR</strong></h5>
            <p class="pmodal"> Sistem informasi layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : Mahasiswa, Admin PPK</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal" data-toggle="modal" data-target="#develop">Login Sistem</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIPAKAR -->

<!-- Start Modal SIMPEL -->
<div id="simpel" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-simpel">
        <h5 class="modal-title">Super User SSO</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIMPEL.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIMPEL</strong></h5>
            <p class="pmodal"> Sistem informasi pelaporan PDPT layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : Admin PDPT, Mahasiswa</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <a href="../../simpel/login/signin.php"><button type="submit" class="btn btn-outline-primary">Login Sistem</button></a>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIMPEL -->

<!-- Start Modal SIPAMAS -->
<div id="sipamas" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-sipamas">
        <h5 class="modal-title">Sistem Informasi Penelitian & Pengabdian Masyarakat</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIPAMAS.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIPAMAS</strong></h5>
            <p class="pmodal"> Sistem informasi layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : Mahasiswa, Admin LPPM</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <a href="../../sipamas/page/front-user"><button type="submit" class="btn btn-outline-primary">Login Sistem
        </button></a>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIPAMAS -->

<!-- Start Modal SIMPKMI -->
<div id="spada" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-spada">
        <h5 class="modal-title">MONEV E-LEARNING</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SPADA.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>MONEV E-LEARNING</strong></h5>
            <p class="pmodal"> Sistem informasi monitoring dan evaluasi pembelajaran daring.</p>
            <p class="pmodal"><strong>Pengguna : Dosen</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <a href="../../spada/"><button type="submit" class="btn btn-outline-primary">Login Sistem
        </button></a>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIMPKMI -->

<!-- Start Modal SIPUS -->
<div id="sipus" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-sipus">
        <h5 class="modal-title">Sistem Informasi Perpustakaan</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIPUS.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIPUS</strong></h5>
            <p class="pmodal"> Sistem informasi layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : Mahasiswa, Admin Perpustakaan</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal" data-toggle="modal" data-target="#develop">Login Sistem</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIPUS -->

<!-- Start Modal Develop -->
<div id="develop" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-up" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row d-flex justify-content-center align-items-center">
          <p class="pt-3 mx-4">
            Maaf! Saat ini sistem masih belum siap untuk digunakan.
          </p>
          <button type="button" class="btn btn-success" data-dismiss="modal">Ok, Saya Mengerti :)</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Develop -->
