<?php
session_start();
  include('../koneksi/koneksi.php');
  // include('../../simawa/page/mhs/login_session.php');
  date_default_timezone_set("Asia/Jakarta");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Metro 4 -->
    <link href="assets/metro-all.css?ver=@@b-version" rel="stylesheet">
    <link href="assets/start.css" rel="stylesheet">
    <link href="assets/modal.css" rel="stylesheet">
    <link href="assets/bootstrap.css" rel="stylesheet">
    <link href="assets/bootstrap.min.css" rel="stylesheet">

    <title>SSO | Universitas Widyagama</title>
    <link href="assets/images/favicon.png" rel="shortcut icon">
  </head>

  <body>
    <div class="container-full-bg">
      <!--jumbotron-->
      <div class="jumbotron jumbotron-fluid text-center" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);">
        <div class="container text-white">
          <h2 class="card-title h2">Single Sign-On | Universitas Widyagama</h2>
          <p class="card-text">Teknologi <em>Single Sign-On</em> (SSO) Universitas Widyagama merupakan teknologi login terpusat dimana sistem yang berbeda dalam satu Universitas dapat terintegrasi dengan satu user <em>account</em> yang valid. </p>
          <button class="button-custom" type="submit" onClick="return false;">Pelajari selengkapnya</button>
        </div>
      </div>

      <div class="container start-custom">
        <div class="tiles-area">
          <div class="tiles-grid tiles-group size-2 fg-white" >
            <div data-role="tile" data-size="medium" style="background-color: #ed3237" data-effect="hover-slide-right" data-toggle="modal" data-target="#simawa">
              <div class="slide-front">
                <img src="assets/images/SIMAWA.jpg" class="h-100 w-100">
              </div>
              <div class="slide-back d-flex flex-justify-center flex-align-center">
                <img src="assets/images/SIMAWA-hover.jpg" class="h-100 w-100">
              </div>
            </div>

            <div data-role="tile" data-size="medium" style="background-color: #66bfa2" data-effect="hover-slide-up" data-toggle="modal" data-target="#simpeg">
              <div class="slide-front">
                <img src="assets/images/SIMPEG.jpg" class="h-100 w-100">
              </div>
              <div class="slide-back d-flex flex-justify-center flex-align-center">
                <img src="assets/images/SIMPEG-hover.jpg" class="h-100 w-100">
              </div>
            </div>

            <div data-role="tile" data-size="medium" style="background-color: #673499" data-effect="hover-slide-down" data-toggle="modal" data-target="#simpel">
              <div class="slide-front">
                <img src="assets/images/SIMPEL.jpg" class="h-100 w-100">
              </div>
              <div class="slide-back d-flex flex-justify-center flex-align-center">
                <img src="assets/images/SIMPEL-hover.jpg" class="h-100 w-100">
              </div>
            </div>

            <div data-role="tile" data-size="medium" style="background-color: #4d9ae3" data-effect="hover-slide-left" data-toggle="modal" data-target="#sipamas">
              <div class="slide-front">
                <img src="assets/images/SIPAMAS.jpg" class="h-100 w-100">
              </div>
              <div class="slide-back d-flex flex-justify-center flex-align-center">
                <img src="assets/images/SIPAMAS-hover.jpg" class="h-100 w-100">
              </div>
            </div>
          </div>

          <div class="tiles-grid tiles-group size-2 fg-white">
            <div data-role="tile" data-size="medium" style="background-color: #5d82a5" data-effect="hover-slide-left" data-toggle="modal" data-target="#siwisu">
              <div class="slide-front">
                <img src="assets/images/SIWISU.jpg" class="h-100 w-100">
              </div>
              <div class="slide-back d-f
              lex flex-justify-center flex-align-center">
                <img src="assets/images/SIWISU-hover.jpg" class="h-100 w-100">
              </div>
            </div>

            <div data-role="tile" data-size="medium" style="background-color: #ffaa31" data-effect="hover-slide-right" data-toggle="modal" data-target="#simas">
              <div class="slide-front">
                <img src="assets/images/SIMAS.jpg" class="h-100 w-100">
              </div>
              <div class="slide-back d-flex flex-justify-center flex-align-center">
                <img src="assets/images/SIMAS-hover.jpg" class="h-100 w-100">
              </div>
            </div>

            <div data-role="tile" data-size="wide">
              <div class="slide-front">
                <a href="https://widyagama.ac.id" target="_blank"><img src="assets/images/SITE.jpg" class="h-100 w-100"></a>
              </div>
            </div>
          </div>

          <div class="tiles-grid tiles-group size-1 fg-white" >
            <div data-role="tile" data-size="medium" style="background-color: #d24627" data-effect="hover-slide-down" data-toggle="modal" data-target="#sipakar">
              <div class="slide-front">
                <img src="assets/images/SIPAKAR.jpg" class="h-100 w-100">
              </div>
              <div class="slide-back d-flex flex-justify-center flex-align-center">
                <img src="assets/images/SIPAKAR-hover.jpg" class="h-100 w-100">
              </div>
            </div>


            <div data-role="tile" data-size="small">
              <a href="https://helpdesk.widyagama.ac.id" target="_blank"><img src="assets/images/Helpdesk.jpg" class="h-100 w-100"></a>
            </div>
            <div data-role="tile" data-size="small">
              <a href="../register/signup.php"><img src="assets/images/Register2.jpg" class="h-100 w-100"></a>
            </div>
            <div data-role="tile" data-size="small">
              <div class="slide-front" >
              <img src="assets/images/super.jpg" class="h-100 w-100">
              </div>
            </div>
            <div data-role="tile" data-size="small">
              <a href="https://catalog.widyagama.ac.id" target="_blank"><img src="assets/images/Digilib.jpg" class="h-100 w-100"></a>
            </div>
          </div>

          <div class="tiles-grid tiles-group size-2 fg-white">
            <div data-role="tile" data-size="wide" >
              <div id="carouselExample1" class="carousel slide z-depth-1-half" data-ride="carousel" style="background-color: #2c3e50">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="assets/images/Informasi.jpg" class="h-100 w-100">
                    </div>
                    <div class="carousel-item">
                      <div class="carousel-content">
                        <p><strong>Sabtu, 5 Januari 2019</strong></br>
                        </br>Dihimbau kepada seluruh mahasiswa untuk segera REGISTER pada SSO untuk dapat menggunakan seluruh layanan SSO Univerisas Widyagama.</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="carousel-content">
                          <p><strong>Rabu, 20 Februari 2019</strong></br>
                          </br>Dihimbau kepada seluruh mahasiswa untuk segera melengkapi BIODATA pada SIMAWA.</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="carousel-content">
                          <p><strong>Jumat, 12 April 2019</strong></br>
                          </br>Bagi mahasiswa yang ingin mengajukan cuti atau aktif dapat mengunduh formulir di SIMAWA submenu CUTI.</p>
                      </div>
                    </div>

                  </div>

                  <a class="carousel-control-prev" href="#carouselExample1" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExample1" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>

            </div>

            <div data-role="tile" data-size="medium" style="background-color: #0f9371" data-effect="hover-slide-up" data-toggle="modal" data-target="#spada">
              <div class="slide-front">
                <img src="assets/images/SPADA.jpg" class="h-100 w-100">
              </div>
              <!-- <div class="slide-back d-flex flex-justify-center flex-align-center">
                <img src="assets/images/SIMPKMI-hover.jpg" class="h-100 w-100">
              </div> -->
            </div>

            <div data-role="tile" data-size="medium" style="background-color: #0e5682" data-effect="hover-slide-right" data-toggle="modal" data-target="#sipus">
              <div class="slide-front">
                <img src="assets/images/SIPUS.jpg" class="h-100 w-100">
              </div>
              <div class="slide-back d-flex flex-justify-center flex-align-center">
                <img src="assets/images/SIPUS-hover.jpg" class="h-100 w-100">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="footer">
      </br>
      <p style="text-align: center;">Copyright &copy; 2019-2020. Powered by <strong>Pusat IT & PDPT Universitas Widyagama</strong></p>
    </div>
      <!-- jQuery first, then Metro UI JS -->
      <script src="assets/jquery-3.3.1.min.js"></script>
      <script src="assets/bootstrap.min.js"></script>
      <script src="assets/bootstrap.js"></script>
      <script src="assets/metro.min.js"></script>
      <script src="assets/start.js"></script>

  </body>
</html>

<!-- Start Modal SIMAWA -->
<div id="simawa" class="modal fade" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-simawa">
        <h5 class="modal-title">Sistem Informasi Mahasiswa</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIMAWA.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIMAWA</strong></h5>
            <p class="pmodal"> Sistem informasi bagi mahasiswa dalam mengkases informasi selama proses perkuliahan seperti biodata, nilai, jadwal kuliah, dan lainnya.</p>
            <p class="pmodal"><strong>Pengguna : Mahasiswa</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal" data-toggle="modal" data-target="#develop">Login Sistem</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIMAWA -->

<!-- Start Modal SIMPEG -->
<div id="simpeg" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-simpeg">
        <h5 class="modal-title">Sistem Informasi Kepegawaian</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIMPEG.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIMPEG</strong></h5>
            <p class="pmodal"> Sistem informasi bagi mahasiswa dalam mengkases informasi selama proses perkuliahan seperti biodata, nilai, jadwal kuliah, dan lainnya.</p>
            <p class="pmodal"><strong>Pengguna : BAUK, Admin Kepegawaian</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <a href="../../simpeg/login/signin.php"><button type="submit" class="btn btn-outline-primary">Login Sistem
        </button></a>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIMPEG -->

<!-- Start Modal SIWISU -->
<div id="siwisu" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-siwisu">
        <h5 class="modal-title">Sistem Informasi Wisuda</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIWISU.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIWISU</strong></h5>
            <p class="pmodal"> Sistem informasi layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : Mahasiswa, Panitia Wisuda</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal" data-toggle="modal" data-target="#develop">Login Sistem</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIWISU -->

<!-- Start Modal SIMAS -->
<div id="simas" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-simas">
        <h5 class="modal-title">Sistem Informasi Assets</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIMAS.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIMAS</strong></h5>
            <p class="pmodal"> Sistem informasi layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : BAUK</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal" data-toggle="modal" data-target="#develop">Login Sistem</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIMAS -->

<!-- Start Modal SIPAKAR -->
<div id="sipakar" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-sipakar">
        <h5 class="modal-title">Sistem Informasi Pengembangan Karir</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIPAKAR.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIPAKAR</strong></h5>
            <p class="pmodal"> Sistem informasi layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : Mahasiswa, Admin PPK</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal" data-toggle="modal" data-target="#develop">Login Sistem</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIPAKAR -->

<!-- Start Modal SIMPEL -->
<div id="simpel" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-simpel">
        <h5 class="modal-title">Super User SSO</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIMPEL.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIMPEL</strong></h5>
            <p class="pmodal"> Sistem informasi pelaporan PDPT layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : Admin PDPT, Mahasiswa</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <a href="../../simpel/login/signin.php"><button type="submit" class="btn btn-outline-primary">Login Sistem</button></a>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIMPEL -->

<!-- Start Modal SIPAMAS -->
<div id="sipamas" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-sipamas">
        <h5 class="modal-title">Sistem Informasi Penelitian & Pengabdian Masyarakat</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIPAMAS.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIPAMAS</strong></h5>
            <p class="pmodal"> Sistem informasi layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : Mahasiswa, Admin LPPM</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <a href="../../sipamas/page/front-user"><button type="submit" class="btn btn-outline-primary">Login Sistem
        </button></a>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIPAMAS -->

<!-- Start Modal SIMPKMI -->
<div id="spada" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-spada">
        <h5 class="modal-title">MONEV E-LEARNING</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SPADA.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>MONEV E-LEARNING</strong></h5>
            <p class="pmodal"> Sistem informasi monitoring dan evaluasi pembelajaran daring.</p>
            <p class="pmodal"><strong>Pengguna : Dosen</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <a href="../../spada/"><button type="submit" class="btn btn-outline-primary">Login Sistem
        </button></a>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIMPKMI -->

<!-- Start Modal SIPUS -->
<div id="sipus" class="modal fade " role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header modal-sipus">
        <h5 class="modal-title">Sistem Informasi Perpustakaan</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5">
            <img src="assets/images/SIPUS.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-7">
            <h5><strong>SIPUS</strong></h5>
            <p class="pmodal"> Sistem informasi layanan akademik penyelenggaran kegiatan yudisium dan wisuda.</p>
            <p class="pmodal"><strong>Pengguna : Mahasiswa, Admin Perpustakaan</strong></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal" data-toggle="modal" data-target="#develop">Login Sistem</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal SIPUS -->

<!-- Start Modal Develop -->
<div id="develop" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-up" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row d-flex justify-content-center align-items-center">
          <p class="pt-3 mx-4">
            Maaf! Saat ini sistem masih belum siap untuk digunakan.
          </p>
          <button type="button" class="btn btn-success" data-dismiss="modal">Ok, Saya Mengerti :)</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Develop -->
