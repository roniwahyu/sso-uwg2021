<?php include "../koneksi/koneksi.php";?>
<!DOCTYPE html>
<html>
<?php include 'struktur/head.php' ?>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <title></title>
  <style type="text/css">
  .fa-input {
    font-family: FontAwesome, 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  </style>
</head>
<title>Sign In | MONEV E-LEARNING</title>
<body class="">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12" >
                <section class="content-header">
                        <h1 class="heading pull-left">
                          Daftar Mata Kuliah
                        
                        </h1>
                        <a href="https://sso.widyagama.ac.id/spada/kelas.php" class="btn pull-right btn-info btn-lg"><i class="fa fa-sign-in"></i> Login Monev Dosen</a>
                        
                      </section>
                    
                      <!-- Main content 1-->
                      <section class="content">
                        <div class="row">
                    
                          <!-- START BOX FILTER JURUSAN -->
                          <?php
                          
                              include "filter-box-admin.php";
                            
                          ?>
                          <!-- END BOX FILTER JURUSAN -->
                          <?php
                          if(isset($_POST['rekap'])){
                            $fakultas = $_POST['fakultas'];
                            $jurusan = $_POST['jurusan'];
                    
                            if ($_POST['fakultas'] == "Semua Fakultas") {
                              $bagianWhere = " (tbl_spada_mk.fak LIKE '%$fakultas%' OR tbl_spada_mk.jur LIKE '%$jurusan%') ORDER BY kode_mk ASC";
                            } else if ($_POST['jurusan'] == "Fakultas Ekonomi dan Bisnis") {
                              $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Ekonomi') ORDER BY kode_mk ASC";
                            } else if ($_POST['jurusan'] == "Fakultas Teknik") {
                              $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Teknik') ORDER BY kode_mk ASC";
                            } else if ($_POST['jurusan'] == "Fakultas Pertanian") {
                              $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Pertanian') ORDER BY kode_mk ASC";
                            } else if ($_POST['jurusan'] == "Fakultas Hukum") {
                              $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Hukum') ORDER BY kode_mk ASC";
                            } else if ($_POST['jurusan'] == "Pascasarjana") {
                              $bagianWhere = " (tbl_spada_mk.fak = 'Pascasarjana') ORDER BY kode_mk ASC";
                            }
                            else {
                              $bagianWhere = " (tbl_spada_mk.fak LIKE '%$fakultas%' AND tbl_spada_mk.jur LIKE '%$jurusan%') ORDER BY kode_mk ASC";
                            }
                    
                    
                            $no = 1;
                            // $sql = "SELECT * FROM tbl_spada_mk JOIN users_nama ON tbl_spada_mk.nidn = users_nama.nidn";
                            $sql = "SELECT * FROM tbl_spada_mk
                            JOIN users_nama ON tbl_spada_mk.nidn = users_nama.nidn
                            WHERE ". $bagianWhere;
                            $data = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
                          ?>
                          <!-- BOX MATA KULIAH -->
                          <div class="col-xs-12" style="padding-top:20px">
                            <div class="box box-sipamas">
                            <div class="box-header with-border text-center">
                              <h3 class="box-title"><b>Daftar Mata Kuliah <?php echo $fakultas . ' ' . $jurusan;?> </b></h3>
                              <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" data-placement="top" title="Tutup"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <div class="box-body">
                              <div class="col-md-12">
                              <div class="table-responsive">
                                <table class="table table-condensed table-bordered table-hover" id="tabel-data">
                                  <thead>
                                    <tr>
                                      <th class="text-center" style="width: 10px">No</th>
                                      <th class="text-center" style="width: 70px">Kode MK</th>
                                      <th class="text-center">Mata Kuliah</th>
                                      <th class="text-center" style="width: 20px">Kode <i>Classroom</i></th>
                                      <th class="text-center" style="width: 180px">NIDN / Nama Dosen</th>
                                     
                                    </tr>
                                  </thead>
                                  <?php
                    
                    
                                  while ($row = mysqli_fetch_array($data)) {
                                    ?>
                                    <tr>
                                      <td class="text-center"><?php echo $no++; ?></td>
                                      <td class="text-center"><?php echo $row['kode_mk'];; ?></td>
                                      <td>
                                        <b><?php echo $row['nama_mk']; ?></b>
                                      </td>
                                      <td class="text-center"><?php echo $row['kode_classroom']; ?></td>
                                     
                                      <td class="text-center"><?php echo $row['nidn'] . " / " . $row['nama_dosen']; ?></td>
                                     
                                    </tr>
                                    <?php
                                  } //akhir while
                    
                                  ?>
                                </table>
                               
                              </div>
                            </div>
                            </div>
                          </div>
                        </div> <!-- col-xs-12 -->
                      <?php } ?>
                      </div>
                      <!-- /.row -->
                    
                    </section>
            </div>  
            <div class="col-xs-12 col-sm-12 col-md-12">
               <p class="footer text-center">Copyright © 2021 <br><strong>Pusat IT & PDPT Universitas Widyagama Malang</strong>.</p>
            </div>
        </div>
</div>
<?php include 'struktur/jquery.php'?>
<script>
$(document).ready(function(){
  var table = $('#tabel-data').DataTable({
    'info'  : true,
    'ordering' : false,
    'pageLength' : 5,
    // dom: 'lBfrtip',
    // buttons: [
    //   'copy', 'csv', 'excel', 'pdf', 'print',
    // ],
    buttons: [
      {
        extend: 'copy',
        text: '<u>C</u>opy',
        exportOptions: {
          columns: ':visible:not(:eq(4))'
        },
        key: {
          key: 'c',
          ctrlkey: true
        }
      },
      {
        extend: 'csv',
        title: 'DAFTAR MATA KULIAH MONEV E LEARNING',
        exportOptions: {
          columns: ':visible:not(:eq(4))'
        }
      },
      {
        extend: 'excel',
        title: 'DAFTAR MATA KULIAH MONEV E LEARNING',
        pageSize: 'A4',
        exportOptions: {
          columns: ':visible:not(:eq(4))'
        }
      },
      {
        extend: 'pdf',
        title: 'DAFTAR MATA KULIAH MONEV E LEARNING',
        pageSize: 'A4',
        orientation: 'landscape',
        exportOptions: {
          columns: ':visible:not(:eq(4))'
        }
      },
      {
        extend: 'print',
        text: '<u>P</u>rint',
        pageSize: 'A4',
        orientation: 'landscape',
        exportOptions: {
          columns: ':visible:not(:eq(4))'
        },
        key: {
          key: 'p',
          ctrlkey: true
        },
        customize: function (win) {
          $(win.document.body).find('table').addClass('display').css('font-size', '12px');
          $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
            $(this).css('background-color','#D0D0D0');
          });
          $(win.document.body).find('h1').css('text-align','center');
        }
      },
    ],
    fixedHeader: {
      header: true,
      footer: true
    },
    'language' : {'emptyTable'  : 'Tidak ada data yang tersedia pada tabel ini',
    'info'        : 'Menampilkan _START_ hingga _END_ dari _TOTAL_ data',
    'infoEmpty'   : 'Menampilkan 0 hingga 0 dari 0 data',
    'infoFiltered': '(dicari dari total _MAX_ data)',
    'search'      : 'Pencarian data:',
    'lengthMenu'  : 'Tampilkan _MENU_ data',
    'zeroRecords' : 'Tidak ditemukan data yang cocok',
    'paginate'    : {
      'first'   : 'Pertama',
      'last'    : 'Terakhir',
      'next'    : 'Selanjutnya',
      'previous': 'Sebelumnya'
    },
  },
});
});
</script>
</body>
</html>
