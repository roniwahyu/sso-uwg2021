
<?php
  include('../sso-uwg/koneksi/koneksi.php');
  date_default_timezone_set("Asia/Jakarta");
?>

<!DOCTYPE html>
<html>
<?php include 'struktur/head.php' ?>
<body class="hold-transition login-page">

<?php
  if(isset($_GET['page'])){
    $page = $_GET['page'];
    switch ($page) {
      case 'login':
        include "login.php";
        break;
      }
    }else{
      include "login.php";
 ?>
<?php include 'struktur/jquery.php'?>
</body>
</html>
