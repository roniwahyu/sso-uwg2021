<?php
include "../../koneksi/koneksi.php";


if (isset($_POST['reset'])) {
  $nidn         = $_POST['nidn'];
  $password     = $_POST['password'];
  $passwordConf = $_POST['password-konf'];

  $cekNIDN = mysqli_num_rows(mysqli_query($koneksi, "SELECT nidn FROM users_nama WHERE nidn LIKE '%$nidn%'"));

  // print_r($cekNIDN);

  if ($cekNIDN > 0) {
    if ($password != $passwordConf) {
      echo "<center><h4><span class='label label-danger'>Password tidak sama!</span><h4></center>";
    }
    else {
      $passwordMd5 = md5($password);
      $queryUpdate = mysqli_query($koneksi, "UPDATE users SET password='$passwordMd5' WHERE username='$nidn'");
      // echo "<center><h4><span class='label label-success'>Password berhasil diubah. Silahkan Sign In.</span><h4></center>";
      header("Location: ../../spada/login/signin.php?msg=success2");
    }
  }
  else {
    echo "<center><h4><span class='label label-danger'>NIDN belum memiliki akun! Silahkan buat akun</span><h4></center>";
  }


}

?>

<html>
<?php include 'struktur/head.php' ?>
<title>Reset Password | MONEV E-LEARNING</title>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <img src="assets/images/favicon.png"  height="85" width="85">
      <h3><b>MONEV E-LEARNING</b><h3>
      </div>
      <div class="login-box-body">
        <h4><p class="login-box-msg head">Reset Password</p></h4>

        <form method="POST" action="">
          <!-- <p>Masukkan email yang digunakan untuk Registrasi.</p> -->
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="nidn" placeholder="NIDN" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="New Password" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password-konf" placeholder="Confirm New Password" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-4">
              <!-- <button type="submit" class="btn btn-primary btn-block btn-flat" name="RESET"><i class="fa fa-mail-forward"></i> Send Link</button> -->
              <button type="submit" class="btn btn-primary btn-block btn-flat" name="reset"><i class="fa fa-lock"> </i> Change</button>
            </div>
          </div>
        </form>
        <a href="signin.php">Sudah ingat password? Masuk di sini.</a><br><br>
        <p class="footer">Copyright © 2019-2020 <br><strong>Powered by Pusat IT & PDPT Universitas Widyagama</strong>.</p>
      </div>
    </div>
    <?php include 'struktur/jquery.php' ?>
  </body>
</html>
