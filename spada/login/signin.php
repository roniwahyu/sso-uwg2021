<?php
session_start();
if($_SESSION){
    if($_SESSION['role']=="admin")
    {
        header("Location: ../page/admin/index.php");
    }
    if($_SESSION['role']=="dosen")
    {
        header("Location: ../page/lecturer/index.php");
    }
}
include('process/process_login.php');

if (isset($_GET['msg']))
{
   $msg=$_GET['msg'];
}
else
{
   $msg="";
}

//siapkan pesan kesalahan
$pesan="";
if ($msg=="success"){
  $pesan='<center><h4><span class="label label-success">Sign Up Berhasil! Silahkan Sign In</span><h4></center>';
}
if ($msg=="success2"){
  $pesan='<center><h4><span class="label label-success">Password berhasil diperbarui! Silahkan Sign In</span><h4></center>';
}
if ($msg=="error1"){
  $pesan='<center><h4><span class="label label-danger">Anda Belum Login!</span><h4></center>';
}
if ($msg=="error2"){
  $pesan='<center><h4><span class="label label-danger">Hak Akses Tidak Sesuai!</span><h4></center>';
}
if ($msg=="error3"){
  $pesan='<center><h4><span class="label label-danger">Username atau Password Salah!</span><h4></center>';
}
if ($msg=="error4"){
  $pesan='<center><h4><span class="label label-danger">Uppss...!!! Login gagal</span><h4></center>';
}

?>


<!DOCTYPE html>
<html>
<?php include 'struktur/head.php' ?>
<title>Sign In | MONEV E-LEARNING</title>
<body class="hold-transition login-page">

 
  
  <div class="login-box" style=""> <!- login-box -->
    <div class="login-logo">
      <img src="assets/images/favicon.png"  height="85" width="85">
      <h3><b>MONEV E-LEARNING</b><h3>
    </div>
    <p> <?php echo $pesan; ?></p>   
    <div class="login-box-body">
    	<h4><p class="login-box-msg head">Sign In</p></h4>

    	<form method="POST">
        <div class="form-group has-feedback">
          <input type="text" class="form-control" name="username" placeholder="NIDN / NUP / NIDK" required>
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
          <input type="password" class="form-control" name="password" placeholder="Password" required>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        
      
           <button type="submit" class="btn btn-danger btn-lg btn-block btn-flat" name="signin"> <i class="fa fa-sign-in"></i> Sign In</button>
      
     
     </form>
     <br>
     <!-- <a href="password_forgot.php">Lupa password</a><br>
     <a href="/sso-uwg/register/signup.php" class="text-center">Anda belum memiliki akun ? Daftar di sini</a><br> -->
     <a href="signup.php">Belum memiliki akun? Daftar di sini.</a><br>
     <a href="lost-password.php">Lupa Password.</a><br>
     <a href="../../landing-page">Beranda SSO.</a><br><br>

        <a href="https://sso.widyagama.ac.id/spada/kelas.php" class="btn btn-block btn-info btn-lg">Data Kelas Dosen</a>
   


   </div>
 </div>

<div class="col-xs-12 col-sm-12 col-md-12">
   <p class="footer text-center">Copyright © 2021 <br><strong>Pusat IT & PDPT Universitas Widyagama Malang</strong>.</p>
</div>
<?php include 'struktur/jquery.php'?>
</body>
</html>
