<?php
  error_reporting(0);
  include 'includes/session.php';

  include('process/process_register.php');

  if (isset($_GET['msg']))
  {
     $msg=$_GET['msg'];
  }
  else
  {
     $msg="";
  }

  //siapkan pesan kesalahan
  $pesan="";
  if ($msg=="error1"){
    $pesan='<center><h4><span class="label label-danger">NIDN / NUP / NIDK sudah terdaftar! Silahkan Sign In</span><h4></center>';
  }
  if ($msg=="error2"){
    $pesan='<center><h4><span class="label label-danger">NIDN / NUP / NIDK belum terdaftar!</span><h4></center>';
  }


?>
<!DOCTYPE html>
<html>
<?php include 'struktur/head.php' ?>
<title>Sign Up | MONEV E-LEARNING</title>

<style type="text/css">
.fa-input {
  font-family: FontAwesome, 'Helvetica Neue', Helvetica, Arial, sans-serif;
}
</style>

<body class="hold-transition register-page">
<div class="register-box">
    <body class="hold-transition login-page">
      <?php echo $pesan; ?>
      <div class="login-logo">
        <img src="assets/images/favicon.png"  height="85" width="85">
        <h3><b>MONEV E-LEARNING </b><h3>
      </div>
  	<div class="register-box-body">
    		<h4><p class="login-box-msg head">Sign Up</p></h4>
    	<form method="POST">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="username" placeholder="NIDN / NUP / NIDK" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
      		<div class="form-group has-feedback">
        		<input type="email" class="form-control" name="email" placeholder="Email" equired>
        		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      		</div>
          <div class="form-group has-feedback">
        		<input type="text" class="form-control" name="telp" placeholder="No. HP/WA" equired>
        		<span class="glyphicon glyphicon-phone form-control-feedback"></span>
      		</div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="Password" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
      		<div class="row">
    			<div class="col-xs-4">
          			<!-- <button type="submit" class="btn btn-primary btn-block btn-flat" name="signup"><i class="fa fa-pencil"></i> Sign Up</button> -->
                <input type="submit" name="signup" value="&#xf040; Sign Up" class="btn btn-primary btn-block btn-flat fa-input" />
        		</div>
      		</div>
    	</form>
      <br>
      <a href="signin.php">Sudah memiliki akun? Masuk di sini.</a><br>
      <a href="lost-password.php">Lupa Password.</a><br><br>

      <p class="footer">Copyright © 2019-2020 <br><strong>Powered by Pusat IT & PDPT Universitas Widyagama</strong>.</p>
  	</div>
</div>
<?php include 'struktur/jquery.php'?>
</body>
</html>
