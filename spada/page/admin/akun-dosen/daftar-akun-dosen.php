<?php
include "../../../../sso-uwg/koneksi/koneksi.php";
?>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <title></title>
  <style type="text/css">
  .fa-input {
    font-family: FontAwesome, 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  </style>
</head>
<body>
  <section class="content-header">
    <h1>
      Daftar Akun Dosen
      <!-- <small>Kegiatan Penelitian</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-user"></i>Daftar Akun Dosen</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-sipamas">
          <!-- <div class="box-header">
          <h3 class="box-title">Data Hari Kerja Kepegawaian Universitas Widyagama Malang</h3>
        </div> -->

        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-condensed table-bordered table-hover" >
            <thead>
              <tr>
                <th class="text-center" style="width: 5px">No</th>
                <th class="text-center" >NIDN / Nama Dosen</th>
                <th class="text-center" style="width: 250px">Fakultas / Jurusan</th>
                <th class="text-center" style="width: 50px">Aksi</th>
              </tr>
            </thead>
            <?php
            $no = 1;

            $a = $_SESSION['username'];

            switch ($a) {
              case 'admin':
                $sql = "SELECT * FROM users JOIN users_nama ON users.username = users_nama.nidn";
                break;
              case 'admin-fe':
                $sql = "SELECT * FROM users JOIN users_nama ON users.username = users_nama.nidn WHERE users_nama.fakultas = 'Fakultas Ekonomi'";
                break;
              case 'admin-fh':
                $sql = "SELECT * FROM users JOIN users_nama ON users.username = users_nama.nidn WHERE users_nama.fakultas = 'Fakultas Hukum'";
                break;
              case 'admin-ft':
                $sql = "SELECT * FROM users JOIN users_nama ON users.username = users_nama.nidn WHERE users_nama.fakultas = 'Fakultas Teknik'";
                break;
              case 'admin-fp':
                $sql = "SELECT * FROM users JOIN users_nama ON users.username = users_nama.nidn WHERE users_nama.fakultas = 'Fakultas Pertanian'";
                break;
              case 'admin-pasca':
                $sql = "SELECT * FROM users JOIN users_nama ON users.username = users_nama.nidn WHERE users_nama.fakultas = 'Pascasarjana'";
                break;
              default:
                $sql = "SELECT * FROM users JOIN users_nama ON users.username = users_nama.nidn";
                break;
            }

            $data = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
            while ($row = mysqli_fetch_array($data)) {
              ?>
              <tr>
                <td class="text-center"><?php echo $no++; ?></td>
                <td ><?php echo $row['nidn'] . " / " . $row['nama_dosen']; ?><br><br>
                  Jumlah Mata Kuliah: <span class='badge bg-navy'><?php
                  $sql2 = "SELECT COUNT(kode_mk) AS num FROM tbl_spada_mk WHERE nidn = '$row[nidn]'";
                  $data2 = mysqli_query($koneksi, $sql2) or die(mysqli_error($koneksi));
                  $row2 = mysqli_fetch_array($data2);
                  echo $row2['num'];
                  ?></span>
                </td>
                <td ><?php echo $row['fakultas'] . " / " . $row['jurusan']; ?></td>

                <td class="text-center">
                  <a class="btn btn-xs btn-success open_modal_password"  data-toggle="tooltip" data-placement="top" title="Ganti Password" id="<?php echo $row['nidn']; ?>"><i class="fa fa-key"></i></a>

                  <?php
                  echo "<a onClick='confirm_delete(\"index.php?page=delete-akun&nidn=$row[nidn]\")' class='btn btn-xs btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus Akun'><i class='fa fa-trash'></i> </a>"; ?>
                </td>
              </tr>

              <?php
            }
            ?>
          </table>
        </div>
      </div>
    </div>
  </div>

</section>

</body>
</html>

<!-- Modal Popup Update Password -->
<div id="ModalEditPassword" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>

<!-- Modal Popup Delete-->
<div id="ModalDelete" class="modal modal-warning fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="text-align:center;">Apakah anda yakin menghapus data ini ?</h4>
      </div>
      <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
        <button type="button" class="btn btn-sm btn-outline" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Kembali</button>
        <a href="#" class="btn btn-sm btn-outline" id="delete_link"><i class="fa fa-trash"></i> Hapus</a>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  var table = $('#tabel-data').DataTable({
    'info'  : true,
    'ordering' : false,
    'pageLength' : 10,
    dom: 'lBfrtip',
    // buttons: [
    //   'copy', 'csv', 'excel', 'pdf', 'print',
    // ],
    buttons: [
      {
        extend: 'copy',
        text: '<u>C</u>opy',
        exportOptions: {
          columns: ':visible:not(:eq(4))'
        },
        key: {
          key: 'c',
          ctrlkey: true
        }
      },
      {
        extend: 'csv',
        title: 'DAFTAR AKUN DOSEN MONEV E-LEARNING',
        exportOptions: {
          columns: ':visible:not(:eq(3))'
        }
      },
      {
        extend: 'excel',
        title: 'DAFTAR AKUN DOSEN MONEV E-LEARNING',
        pageSize: 'A4',
        exportOptions: {
          columns: ':visible:not(:eq(3))'
        }
      },
      {
        extend: 'pdf',
        title: 'DAFTAR AKUN DOSEN MONEV E-LEARNING',
        pageSize: 'A4',
        orientation: 'portrait',
        exportOptions: {
          columns: ':visible:not(:eq(3))'
        }
      },
      {
        extend: 'print',
        text: '<u>P</u>rint',
        pageSize: 'A4',
        orientation: 'landscape',
        exportOptions: {
          columns: ':visible:not(:eq(3))'
        },
        key: {
          key: 'p',
          ctrlkey: true
        },
        customize: function (win) {
          $(win.document.body).find('table').addClass('display').css('font-size', '12px');
          $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
            $(this).css('background-color','#D0D0D0');
          });
          $(win.document.body).find('h1').css('text-align','center');
        }
      },
    ],
    fixedHeader: {
      header: true,
      footer: true
    },
    'language' : {'emptyTable'  : 'Tidak ada data yang tersedia pada tabel ini',
    'info'        : 'Menampilkan _START_ hingga _END_ dari _TOTAL_ data',
    'infoEmpty'   : 'Menampilkan 0 hingga 0 dari 0 data',
    'infoFiltered': '(dicari dari total _MAX_ data)',
    'search'      : 'Pencarian data:',
    'lengthMenu'  : 'Tampilkan _MENU_ data',
    'zeroRecords' : 'Tidak ditemukan data yang cocok',
    'paginate'    : {
      'first'   : 'Pertama',
      'last'    : 'Terakhir',
      'next'    : 'Selanjutnya',
      'previous': 'Sebelumnya'
    },
  },
});
});
</script>

<!-- Javascript Update Password-->
<script>
$(document).ready(function () {
  $(".open_modal_password").click(function(edit) {
    var o = $(this).attr("id");
    $.ajax({
      url: "akun-dosen/modal-password-update.php",
      type: "GET",
      data : {nidn: o,},
      success: function (ajaxData){
        $("#ModalEditPassword").html(ajaxData);
        $("#ModalEditPassword").modal('show',{backdrop: 'true'});
      }
    });
  });
});
</script>

<!-- Javascript Delete -->
<script>
function confirm_delete(delete_url){
  $("#ModalDelete").modal('show', {backdrop: 'static'});
  document.getElementById('delete_link').setAttribute('href', delete_url);
}
</script>
