<?php
// include "../../../../sso-uwg/koneksi/koneksi.php";
// include "../../../../sso-uwg/enkripsi.php";

// php qr code
include "../../../../sso-uwg/assets/plugins/phpqrcode/qrlib.php";

// memanggil library FPDF
require('../../../../sso-uwg/assets/plugins/fpdf/fpdf.php');

function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}


$fak = $_GET['fak'];
$jur = $_GET['jur'];


if ($fak == "Semua Fakultas") {
  $bagianWhere = " (tbl_spada_mk.fak LIKE '%$fak%' OR tbl_spada_mk.jur LIKE '%$jur%') ORDER BY kode_mk ASC";
} else if ($jur == "Fakultas Ekonomi dan Bisnis") {
  $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Ekonomi dan Bisnis') ORDER BY kode_mk ASC";
} else if ($jur == "Fakultas Teknik") {
  $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Teknik') ORDER BY kode_mk ASC";
} else if ($jur == "Fakultas Pertanian") {
  $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Pertanian') ORDER BY kode_mk ASC";
} else if ($jur == "Fakultas Hukum") {
  $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Hukum') ORDER BY kode_mk ASC";
} else if ($jur == "Pascasarjana") {
  $bagianWhere = " (tbl_spada_mk.fak = 'Pascasarjana') ORDER BY kode_mk ASC";
}
else {
  $bagianWhere = " (tbl_spada_mk.fak LIKE '%$fak%' AND tbl_spada_mk.jur LIKE '%$jur%') ORDER BY kode_mk ASC";
}


// intance object dan memberikan pengaturan halaman PDF
$pdf = new FPDF('l','mm','A4');
// membuat halaman baru
$pdf->AddPage();
// setting jenis font yang akan digunakan
$pdf->SetFont('Arial','B',15);
// mencetak string
$pdf->Cell(280,7,'DAFTAR MATA KULIAH',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(280,5, $fak . ' ' . $jur,0,1,'C');

// Memberikan space kebawah agar tidak terlalu rapat
// // $pdf->Cell(10,7,'',0,1);
// $pdf->SetFont('Arial','I',10);
// $pdf->Cell(275,7, 'Laporan digenerate dari sso.widyagama.org/spada pada ' . date('d-M-Y h:i:s A'),0,1,'');

$pdf->Cell(10,6,'',0,1); //space

$pdf->SetFont('Arial','B',10);
$pdf->Cell(8,10,'No',1,0,'C');
$pdf->Cell(30,10,'Kode MK',1,0,'C');
$pdf->Cell(110,10,'Nama Mata Kuliah',1,0,'C');
$pdf->Cell(20,10,'Kode GC',1,0,'C');
$pdf->Cell(108,10,'NIDN / Nama Dosen',1,1,'C');

$pdf->SetFont('Arial','',9);

$no = 1;
$sql = mysqli_query($koneksi, "SELECT * FROM tbl_spada_mk
  JOIN users_nama ON tbl_spada_mk.nidn = users_nama.nidn
  WHERE ". $bagianWhere);

  while ($row = mysqli_fetch_array($sql)){
    $cellWidth=110; //lebar sel
    $cellHeight=6; //tinggi sel satu baris normal

    //periksa apakah teksnya melibihi kolom?
    if($pdf->GetStringWidth($row['nama_mk']) < $cellWidth){
      //jika tidak, maka tidak melakukan apa-apa
      $line=1;
    }else{
      //jika ya, maka hitung ketinggian yang dibutuhkan untuk sel akan dirapikan
      //dengan memisahkan teks agar sesuai dengan lebar sel
      //lalu hitung berapa banyak baris yang dibutuhkan agar teks pas dengan sel

      $textLength=strlen($row['nama_mk']);	//total panjang teks
      $errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
      $startChar=0;		//posisi awal karakter untuk setiap baris
      $maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
      $textArray=array();	//untuk menampung data untuk setiap baris
      $tmpString="";		//untuk menampung teks untuk setiap baris (sementara)

      while($startChar < $textLength){ //perulangan sampai akhir teks
        //perulangan sampai karakter maksimum tercapai
        while(
          $pdf->GetStringWidth( $tmpString ) < ($cellWidth-$errMargin) &&
          ($startChar+$maxChar) < $textLength ) {
            $maxChar++;
            $tmpString=substr($row['nama_mk'],$startChar,$maxChar);
          }
          //pindahkan ke baris berikutnya
          $startChar=$startChar+$maxChar;
          //kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
          array_push($textArray,$tmpString);
          //reset variabel penampung
          $maxChar=0;
          $tmpString='';

        }
        //dapatkan jumlah baris
        $line=count($textArray);
      }

      //tulis selnya
      $pdf->SetFillColor(255,255,255);
      $pdf->Cell(8,($line * $cellHeight),$no++,1,0,'C',true); //sesuaikan ketinggian dengan jumlah garis
      $pdf->Cell(30,($line * $cellHeight),$row['kode_mk'],1,0); //sesuaikan ketinggian dengan jumlah garis

      //memanfaatkan MultiCell sebagai ganti Cell
      //atur posisi xy untuk sel berikutnya menjadi di sebelahnya.
      //ingat posisi x dan y sebelum menulis MultiCell
      $xPos=$pdf->GetX();
      $yPos=$pdf->GetY();
      $pdf->MultiCell($cellWidth,$cellHeight,$row['nama_mk'],1,'L');

      //kembalikan posisi untuk sel berikutnya di samping MultiCell
      //dan offset x dengan lebar MultiCell
      $pdf->SetXY($xPos + $cellWidth , $yPos);
      $pdf->Cell(20,($line * $cellHeight),$row['kode_classroom'],1,0); //sesuaikan ketinggian dengan jumlah garis

      $pdf->Cell(108,($line * $cellHeight),$row['nidn'] . " / " . $row['nama_dosen'],1,1); //sesuaikan ketinggian dengan jumlah garis
    }


        $tempdir = "temp/"; //Nama folder tempat menyimpan file qrcode
        if (!file_exists($tempdir)) //Buat folder bername temp
           mkdir($tempdir);
           $quality = 'H';
           //Ukuran besar QRCode
           $ukuran = 2;
           $padding =1;
           //isi qrcode jika di scan
           $codeContents = 'https://sso.widyagama.ac.id/spada/page/view-report/index.php?view=daftar-mk&fak=' . base64_encode($fak) . '&jur=' . base64_encode($jur);
           $namafile = "Laporan - ". $fak . ' ' . $jur . ' (' . date('d-M-Y') .')' .".png";

        //simpan file kedalam folder temp dengan nama 001.png
        QRcode::png($codeContents,$tempdir.$namafile,$quality,$ukuran,$padding);
        $qr = "temp/".$namafile;

    // $pdf->Image('temp/'.$namafile,0,0);
    $pdf->Cell(10,4,'',0,1);
    $pdf->Cell(0,0, $pdf->Image($qr),0,1,'C');

    $pdf->SetFont('Arial','I',10);
    $pdf->Cell(275,6, 'Laporan digenerate dari sso.widyagama.ac.id/spada pada ' . date('d-M-Y h:i:s A'),0,1,'');

    $pdf->Output('I', 'Daftar Mata Kuliah ' . $fak . ' ' . $jur .'.pdf');
    ?>
