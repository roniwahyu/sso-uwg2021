<?php
// include "../../../../sso-uwg/koneksi/koneksi.php";
// include "../../../../sso-uwg/enkripsi.php";

function tgl_indo($tanggal)
{
  $bulan = array(
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

?>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <title></title>
  <style type="text/css">
  .fa-input {
    font-family: FontAwesome, 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  </style>
</head>
<body>
  <section class="content-header">
    <h1>
      Daftar Mata Kuliah
      <!-- <small>Kegiatan Penelitian</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-file-o"></i>Daftar MK</a></li>
    </ol>
  </section>

  <!-- Main content 1-->
  <section class="content">
    <div class="row">

      <!-- START BOX FILTER JURUSAN -->
      <?php
      $a = $_SESSION['username'];

      switch ($a) {
        case 'admin':
          include "filter-box/filter-box-admin.php";
          break;
        case 'admin-fe':
          include "filter-box/filter-box-admin-fe.php";
          break;
        case 'admin-ft':
          include "filter-box/filter-box-admin-ft.php";
          break;
        case 'admin-fp':
          include "filter-box/filter-box-admin-fp.php";
          break;
        case 'admin-fh':
          include "filter-box/filter-box-admin-fh.php";
          break;
        case 'admin-pasca':
          include "filter-box/filter-box-admin-pasca.php";
          break;
        default:
          include "filter-box/filter-box-admin.php";
          break;
      }
      ?>
      <!-- END BOX FILTER JURUSAN -->
      <?php
      if(isset($_POST['rekap'])){
        $fakultas = $_POST['fakultas'];
        $jurusan = $_POST['jurusan'];

        if ($_POST['fakultas'] == "Semua Fakultas") {
          $bagianWhere = " (tbl_spada_mk.fak LIKE '%$fakultas%' OR tbl_spada_mk.jur LIKE '%$jurusan%') ORDER BY kode_mk ASC";
        } else if ($_POST['jurusan'] == "Fakultas Ekonomi dan Bisnis") {
          $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Ekonomi') ORDER BY kode_mk ASC";
        } else if ($_POST['jurusan'] == "Fakultas Teknik") {
          $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Teknik') ORDER BY kode_mk ASC";
        } else if ($_POST['jurusan'] == "Fakultas Pertanian") {
          $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Pertanian') ORDER BY kode_mk ASC";
        } else if ($_POST['jurusan'] == "Fakultas Hukum") {
          $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Hukum') ORDER BY kode_mk ASC";
        } else if ($_POST['jurusan'] == "Pascasarjana") {
          $bagianWhere = " (tbl_spada_mk.fak = 'Pascasarjana') ORDER BY kode_mk ASC";
        }
        else {
          $bagianWhere = " (tbl_spada_mk.fak LIKE '%$fakultas%' AND tbl_spada_mk.jur LIKE '%$jurusan%') ORDER BY kode_mk ASC";
        }


        $no = 1;
        // $sql = "SELECT * FROM tbl_spada_mk JOIN users_nama ON tbl_spada_mk.nidn = users_nama.nidn";
        $sql = "SELECT * FROM tbl_spada_mk
        JOIN users_nama ON tbl_spada_mk.nidn = users_nama.nidn
        WHERE ". $bagianWhere;
        $data = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
      ?>
      <!-- BOX MATA KULIAH -->
      <div class="col-xs-12">
        <div class="box box-sipamas">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><b>Daftar Mata Kuliah <?php echo $fakultas . ' ' . $jurusan;?> </b></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" data-placement="top" title="Tutup"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="tabel-data">
              <thead>
                <tr>
                  <th class="text-center" style="width: 10px">No</th>
                  <th class="text-center" style="width: 70px">Kode MK</th>
                  <th class="text-center">Mata Kuliah</th>
                  <th class="text-center" style="width: 20px">Kode <i>Classroom</i></th>
                  <th class="text-center" style="width: 20px">Jumlah Pertemuan</th>
                  <th class="text-center" style="width: 180px">NIDN / Nama Dosen</th>
                  <th class="text-center" style="width: 20px">Aksi</th>
                </tr>
              </thead>
              <?php


              while ($row = mysqli_fetch_array($data)) {
                ?>
                <tr>
                  <td class="text-center"><?php echo $no++; ?></td>
                  <td class="text-center"><?php echo $row['kode_mk'];; ?></td>
                  <td>
                    <b><?php echo $row['nama_mk']; ?></b>
                    <?php if(!empty($row['file_rps'])):?>
                    <a href="https://docs.google.com/gview?url=https://sso.widyagama.ac.id/spada/up_rps/<?php echo $row['file_rps']; ?>&embedded=true" target="_blank" class="btn btn-xs btn-danger"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;Lihat RPS</a>
                    <?php endif; ?>
                    
                  </td>
                  <td class="text-center"><?php echo $row['kode_classroom']; ?></td>
                  <td class="text-center">
                    <?php
                    $sql2 = "SELECT COUNT(kode_mk) AS num FROM tbl_spada_pertemuan WHERE (kode_mk = '$row[kode_mk]' AND nidn = '$row[nidn]')";
                    $data2 = mysqli_query($koneksi, $sql2) or die(mysqli_error($koneksi));
                    $row2 = mysqli_fetch_array($data2);
                    echo $row2['num'];
                    ?>

                  </td>
                  <td class="text-center"><?php echo $row['nidn'] . " / " . $row['nama_dosen']; ?></td>
                  <td class="text-center">
                    <a href="index.php?page=pertemuan&nidn=<?php echo $row['nidn'];?>&kode_mk=<?php echo $row['kode_mk'];?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" title="Lihat Pertemuan" id="<?php echo $row['id_pe'];?>" target="_blank"><i class="fa fa-info-circle"></i> </a>
                  </td>
                </tr>
                <?php
              } //akhir while

              ?>
            </table>
            
            <div class="text-left">
              <br>
              <a href="print.php?p=daftar-mk&fak=<?php echo $fakultas;?>&jur=<?php echo $jurusan;?>" type="button" name="tambah" id="tambah" data-toggle="tooltip" data-target="#ModalAdd" class="btn btn-sm btn-primary open_modal_add" target="_blank"><i class="fa fa-print"></i> Cetak laporan </a>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div> <!-- col-xs-12 -->
  <?php } ?>
  </div>
  <!-- /.row -->

</section>

</body>
</html>


<script>
$(document).ready(function(){
  var table = $('#tabel-data').DataTable({
    'info'  : true,
    'ordering' : false,
    'pageLength' : 5,
    // dom: 'lBfrtip',
    // buttons: [
    //   'copy', 'csv', 'excel', 'pdf', 'print',
    // ],
    buttons: [
      {
        extend: 'copy',
        text: '<u>C</u>opy',
        exportOptions: {
          columns: ':visible:not(:eq(4))'
        },
        key: {
          key: 'c',
          ctrlkey: true
        }
      },
      {
        extend: 'csv',
        title: 'DAFTAR MATA KULIAH MONEV E LEARNING',
        exportOptions: {
          columns: ':visible:not(:eq(4))'
        }
      },
      {
        extend: 'excel',
        title: 'DAFTAR MATA KULIAH MONEV E LEARNING',
        pageSize: 'A4',
        exportOptions: {
          columns: ':visible:not(:eq(4))'
        }
      },
      {
        extend: 'pdf',
        title: 'DAFTAR MATA KULIAH MONEV E LEARNING',
        pageSize: 'A4',
        orientation: 'landscape',
        exportOptions: {
          columns: ':visible:not(:eq(4))'
        }
      },
      {
        extend: 'print',
        text: '<u>P</u>rint',
        pageSize: 'A4',
        orientation: 'landscape',
        exportOptions: {
          columns: ':visible:not(:eq(4))'
        },
        key: {
          key: 'p',
          ctrlkey: true
        },
        customize: function (win) {
          $(win.document.body).find('table').addClass('display').css('font-size', '12px');
          $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
            $(this).css('background-color','#D0D0D0');
          });
          $(win.document.body).find('h1').css('text-align','center');
        }
      },
    ],
    fixedHeader: {
      header: true,
      footer: true
    },
    'language' : {'emptyTable'  : 'Tidak ada data yang tersedia pada tabel ini',
    'info'        : 'Menampilkan _START_ hingga _END_ dari _TOTAL_ data',
    'infoEmpty'   : 'Menampilkan 0 hingga 0 dari 0 data',
    'infoFiltered': '(dicari dari total _MAX_ data)',
    'search'      : 'Pencarian data:',
    'lengthMenu'  : 'Tampilkan _MENU_ data',
    'zeroRecords' : 'Tidak ditemukan data yang cocok',
    'paginate'    : {
      'first'   : 'Pertama',
      'last'    : 'Terakhir',
      'next'    : 'Selanjutnya',
      'previous': 'Sebelumnya'
    },
  },
});
});
</script>
