<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<div class="col-xs-12">
  <div class="box box-sipamas">
    <div class="box-body">
      <form method="POST" class="form-user" id="form-user">
        <div class="col-md-12">
          <label>Program Studi</label>
          <select name="jurusan" id="jurusan" class="form-control form-control-sm" required>
            <option value=""> --- Pilih Program Studi --- </option>
            <option value="Fakultas Teknik">Semua Program Studi</option>
            <option disabled>----</option>
            <option value="D3 Otomotif">D3 Otomotif</option>
            <option value="S1 Teknik Elektro">S1 Teknik Elektro</option>
            <option value="S1 Teknik Informatika">S1 Teknik Informatika</option>
            <option value="S1 Teknik Mesin">S1 Teknik Mesin</option>
            <option value="S1 Teknik Industri">S1 Teknik Industri</option>
            <option value="S1 Teknik Sipil">S1 Teknik Sipil</option>
          </select>
        </div>
        <div class="col-md-12">
          <br>
          <button type="submit" class="btn btn-sm btn-primary" id="tombol-rekap" name="rekap"><span class="glyphicon glyphicon-eye-open"></span> Tampilkan Data </button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
$('#reservation').daterangepicker({
  autoUpdateInput: false
}).on("apply.daterangepicker", function (e, picker) {
  picker.element.val(picker.startDate.format(picker.locale.format) + ' - ' + picker.endDate.format(picker.locale.format));
});
</script>
