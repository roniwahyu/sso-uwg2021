<?php
  include('../../../../sso-uwg/koneksi/koneksi.php');
  include_once('../../../../sso-uwg/enkripsi.php');
  include('login_session.php');
  date_default_timezone_set("Asia/Jakarta");
?>

<!DOCTYPE html>
<html>
<?php include "../../struktur/head.php"; ?>
<body class="hold-transition skin-sipamas sidebar-mini">
<div class="wrapper">

  <?php include "../../struktur/header_admin.php" ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "../../struktur/menu_admin.php" ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <?php

        if(isset($_GET['page'])){
          $page = $_GET['page'];
          //----------START ADMIN PAGE----------//
          switch ($page) {
            // ---- START REKAP PERTEMUAN MATA KULIAH ----//
            case 'rekap-pertemuan-mk':
              include 'rekap-pertemuan-mk/rekap-mata-kuliah.php';
              break;
            // view
            case 'pertemuan':
              include 'rekap-pertemuan-mk/pertemuan-mata-kuliah.php';
              break;
            // ---- END REKAP PERTEMUAN MATA KULIAH ----//

            // ---- START DAFTAR MATA KULIAH ----//
            case 'daftar-mk':
              include 'daftar-mata-kuliah/daftar-mata-kuliah.php';
              break;
            // ---- END DAFTAR MATA KULIAH ----//

            // ---- START DAFTAR AKUN DOSEN ----//
            case 'akun-dosen':
              include 'akun-dosen/daftar-akun-dosen.php';
              break;
            case 'password-update':
              include 'akun-dosen/manage-akun-dosen/password-update.php';
              break;
            case 'delete-akun':
              include 'akun-dosen/manage-akun-dosen/akun-dosen-delete.php';
              break;
            // ---- END DAFTAR AKUN DOSEN ----//

            // ---- START PANDUAN ----//
            case 'panduan':
              include 'panduan/panduan-admin.php';
              break;
            // ---- END PANDUAN ----//

            default:
              include "notFound.php";
              break;
          }
        }else{
          include "rekap-pertemuan-mk/rekap-mata-kuliah.php";
        }
        //----------END ADMIN PAGE----------//

     ?>


  </div>
  <!-- /.content-wrapper -->
  <?php include '../../struktur/footer.php'; ?>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<?php include '../../struktur/jquery.php'; ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-162978976-7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-162978976-7');
</script>

</body>

</html>
