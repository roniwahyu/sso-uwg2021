<?php
include "../../../../sso-uwg/koneksi/koneksi.php";
// include "../../../../sso-uwg/enkripsi.php";

// memanggil library FPDF
require('../../../../sso-uwg/assets/plugins/fpdf/fpdf.php');

function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}


$fak = $_GET['fak'];
$jur = $_GET['jur'];
$tgl = $_GET['tgl'];

$pecah_tanggal  = explode('-',$tgl);
$tanggal_awal   = date('Y-m-d',strtotime($pecah_tanggal[0]));
$tanggal_akhir  = date('Y-m-d',strtotime($pecah_tanggal[1]));

if ($fak == "Semua Fakultas") {
  $bagianWhere = " (tbl_spada_mk.fak LIKE '%$fak%' OR tbl_spada_mk.jur LIKE '%$jur%') AND (tbl_spada_pertemuan.tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir') GROUP BY nama_mk ORDER BY users_nama.nama_dosen ASC";
} else if ($jur == "Fakultas Ekonomi dan Bisnis") {
  $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Ekonomi dan Bisnis') AND (tbl_spada_pertemuan.tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir') GROUP BY nama_mk ORDER BY users_nama.nama_dosen ASC";
} else if ($jur == "Fakultas Teknik") {
  $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Teknik') AND (tbl_spada_pertemuan.tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir') GROUP BY nama_mk ORDER BY users_nama.nama_dosen ASC";
} else if ($jur == "Fakultas Pertanian") {
  $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Pertanian') AND (tbl_spada_pertemuan.tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir') GROUP BY nama_mk ORDER BY users_nama.nama_dosen ASC";
} else if ($jur == "Fakultas Hukum") {
  $bagianWhere = " (tbl_spada_mk.fak = 'Fakultas Hukum') AND (tbl_spada_pertemuan.tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir') GROUP BY nama_mk ORDER BY users_nama.nama_dosen ASC";
} else if ($jur == "Pascasarjana") {
  $bagianWhere = " (tbl_spada_mk.fak = 'Pascasarjana') AND (tbl_spada_pertemuan.tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir') GROUP BY nama_mk ORDER BY users_nama.nama_dosen ASC";
}
else {
  $bagianWhere = " (tbl_spada_mk.fak LIKE '%$fak%' AND tbl_spada_mk.jur LIKE '%$jur%') AND (tbl_spada_pertemuan.tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir') GROUP BY nama_mk ORDER BY users_nama.nama_dosen ASC";
}


// intance object dan memberikan pengaturan halaman PDF
$pdf = new FPDF('l','mm','A4');
// membuat halaman baru
$pdf->AddPage();
// setting jenis font yang akan digunakan
$pdf->SetFont('Arial','B',15);
// mencetak string
$pdf->Cell(280,7,'REKAP JUMLAH PERTEMUAN DOSEN TIAP MATA KULIAH',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(275,7, $fak . ' ' . $jur,0,1,'C');
$pdf->Cell(275,3,'Per Tanggal ' . tgl_indo($tanggal_awal) . ' - ' . tgl_indo($tanggal_akhir),0,1,'C');

// Memberikan space kebawah agar tidak terlalu rapat
$pdf->Cell(10,7,'',0,1);
$pdf->SetFont('Arial','I',10);
$pdf->Cell(275,7, 'Laporan digenerate dari sso.widyagama.org/spada pada ' . date('d-M-Y h:i:s A'),0,1,'');

$pdf->Cell(10,2,'',0,1); //space

$pdf->SetFont('Arial','B',10);
$pdf->Cell(8,10,'No',1,0,'C');
$pdf->Cell(30,10,'Kode MK',1,0,'C');
$pdf->Cell(110,10,'Nama Mata Kuliah',1,0,'C');
$pdf->Cell(20,10,'Kode GC',1,0,'C');
$pdf->Cell(10,10,'Jml',1,0,'C');
$pdf->Cell(100,10,'NIDN / Nama Dosen',1,1,'C');

$pdf->SetFont('Arial','',9);

$no = 1;
$querysql="SELECT * FROM tbl_spada_mk
  JOIN users_nama ON tbl_spada_mk.nidn = users_nama.nidn
  JOIN tbl_spada_pertemuan ON tbl_spada_mk.kode_mk = tbl_spada_pertemuan.kode_mk
  WHERE ". $bagianWhere; //update SWI 20102020 untuk keperluan investigasi masalah data tidak dapat dicetak
$sql = mysqli_query($koneksi,$querysql);
// print_r($sql);
  while ($row = mysqli_fetch_array($sql)){
  $sql2 = mysqli_query($koneksi, "SELECT COUNT(kode_mk) AS num FROM tbl_spada_pertemuan WHERE (kode_mk = '$row[kode_mk]' AND nidn = '$row[nidn]') AND (tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir')");
  $row2 = mysqli_fetch_array($sql2);
// print_r($row);
// print_r($row2);
    $cellWidth=110; //lebar sel
    $cellHeight=6; //tinggi sel satu baris normal

    //periksa apakah teksnya melibihi kolom?
    if($pdf->GetStringWidth($row['nama_mk']) < $cellWidth){
      //jika tidak, maka tidak melakukan apa-apa
      $line=1;
    }else{
      //jika ya, maka hitung ketinggian yang dibutuhkan untuk sel akan dirapikan
      //dengan memisahkan teks agar sesuai dengan lebar sel
      //lalu hitung berapa banyak baris yang dibutuhkan agar teks pas dengan sel

      $textLength=strlen($row['nama_mk']);	//total panjang teks
      $errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
      $startChar=0;		//posisi awal karakter untuk setiap baris
      $maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
      $textArray=array();	//untuk menampung data untuk setiap baris
      $tmpString="";		//untuk menampung teks untuk setiap baris (sementara)

      while($startChar < $textLength){ //perulangan sampai akhir teks
        //perulangan sampai karakter maksimum tercapai
        while(
          $pdf->GetStringWidth( $tmpString ) < ($cellWidth-$errMargin) &&
          ($startChar+$maxChar) < $textLength ) {
            $maxChar++;
            $tmpString=substr($row['nama_mk'],$startChar,$maxChar);
          }
          //pindahkan ke baris berikutnya
          $startChar=$startChar+$maxChar;
          //kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
          array_push($textArray,$tmpString);
          //reset variabel penampung
          $maxChar=0;
          $tmpString='';

        }
        //dapatkan jumlah baris
        $line=count($textArray);
      }

      //tulis selnya
      $pdf->SetFillColor(255,255,255);
      $pdf->Cell(8,($line * $cellHeight),$no++,1,0,'C',true); //sesuaikan ketinggian dengan jumlah garis
      $pdf->Cell(30,($line * $cellHeight),$row['kode_mk'],1,0); //sesuaikan ketinggian dengan jumlah garis

      //memanfaatkan MultiCell sebagai ganti Cell
      //atur posisi xy untuk sel berikutnya menjadi di sebelahnya.
      //ingat posisi x dan y sebelum menulis MultiCell
      $xPos=$pdf->GetX();
      $yPos=$pdf->GetY();
      $pdf->MultiCell($cellWidth,$cellHeight,$row['nama_mk'],1,'L');

      //kembalikan posisi untuk sel berikutnya di samping MultiCell
      //dan offset x dengan lebar MultiCell
      $pdf->SetXY($xPos + $cellWidth , $yPos);
      $pdf->Cell(20,($line * $cellHeight),$row['kode_classroom'],1,0); //sesuaikan ketinggian dengan jumlah garis
      $pdf->Cell(10,($line * $cellHeight),$row2['num'],1,0,'C'); //sesuaikan ketinggian dengan jumlah garis

      $pdf->Cell(100,($line * $cellHeight),$row['nidn'] . " / " . $row['nama_dosen'],1,1); //sesuaikan ketinggian dengan jumlah garis
    }

    $pdf->Output('I', 'Rekap Pertemuan Dosen Tiap Mata Kuliah ' . $fak . ' ' . $jur . ' Per ' . tgl_indo($tanggal_awal) . ' - ' . tgl_indo($tanggal_akhir) .'.pdf');
    ?>
