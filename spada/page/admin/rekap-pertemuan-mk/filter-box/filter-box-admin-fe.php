<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<div class="col-xs-12">
  <div class="box box-sipamas">
    <div class="box-body">
      <form method="POST" class="form-user" id="form-user">
        <div class="col-md-12">
          <label>Program Studi</label>
          <select name="jurusan" id="jurusan" class="form-control form-control-sm">
            <option value=""> --- Pilih Program Studi --- </option>
            <option value="Fakultas Ekonomi">Semua Program Studi</option>
            <option disabled>----</option>
            <option value="D3 Keuangan & Perbankan">D3 Keuangan & Perbankan</option>
            <option value="S1 Akuntansi">S1 Akuntansi</option>
            <option value="S1 Manajemen">S1 Manajemen</option>
          </select>
        </div>
        <div class="col-md-12">
          <label>Tangal Pertemuan:</label>
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control " name="range_tanggal" id="reservation" placeholder="--- Pilih Tanggal Pertemuan ---" required autocomplete="off">
          </div>
        </div>
        <div class="col-md-12">
          <br>
          <button type="submit" class="btn btn-sm btn-primary" id="tombol-rekap" name="rekap"><span class="glyphicon glyphicon-eye-open"></span> Tampilkan Data </button>
          <br><br>
          <p class="text-muted">Menampilkan jumlah pertemuan mata kuliah pada rentang tanggal tertentu. Mata Kuliah yang tidak ada pertemuan <b>(jumlah = 0) tidak ditampilkan</b>.</p>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
$('#reservation').daterangepicker({
  autoUpdateInput: false
}).on("apply.daterangepicker", function (e, picker) {
  picker.element.val(picker.startDate.format(picker.locale.format) + ' - ' + picker.endDate.format(picker.locale.format));
});
</script>
