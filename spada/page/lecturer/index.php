<?php
  include('../../../../sso-uwg/koneksi/koneksi.php');
  include('login_session.php');
  date_default_timezone_set("Asia/Jakarta");
?>

<!DOCTYPE html>
<html>
<?php include "../../struktur/head.php"; ?>
<body class="hold-transition skin-sipamas sidebar-mini">
<div class="wrapper">

  <?php include "../../struktur/header_lecturer.php" ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "../../struktur/menu_lecturer.php" ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <?php

        if(isset($_GET['page'])){
          $page = $_GET['page'];
          //----------START ADMIN PAGE----------//
          switch ($page) {
            // ---- START MATA KULIAH ----//
            case 'mata-kuliah':
              include 'mata-kuliah/daftar-mata-kuliah.php';
              break;
            // view pertemuan mata KULIAH
            case 'pertemuan':
              include 'mata-kuliah/pertemuan-mata-kuliah.php';
              break;
            // Upload RPS
            case 'up-rps':
              include 'mata-kuliah/modal-rps/proses-add-rps.php';
              break;
            // manage mata kuliah
            case 'mk-add':
              include 'mata-kuliah/manage-mata-kuliah/mk-add.php'; // ADD
              break;
            case 'mk-update':
              include 'mata-kuliah/manage-mata-kuliah/mk-update.php'; // UPDATE
              break;
            case 'mk-delete':
              include 'mata-kuliah/manage-mata-kuliah/mk-delete.php'; // DELETE
              break;
            case 'mk-truncate':
              include 'mata-kuliah/manage-mata-kuliah/mk-truncate.php'; // TRUNCATE
              break;
            // manage pertemuan mata kuliah
            case 'pertemuan-add':
              include 'mata-kuliah/manage-pertemuan/pertemuan-add.php'; // ADD
              break;
            case 'pertemuan-update':
              include 'mata-kuliah/manage-pertemuan/pertemuan-update.php'; // UPDATE
              break;
            case 'pertemuan-delete':
              include 'mata-kuliah/manage-pertemuan/pertemuan-delete.php'; // DELETE
              break;
            case 'pertemuan-truncate':
              include 'mata-kuliah/manage-pertemuan/pertemuan-truncate.php'; // TRUNCATE
              break;
            // ---- END MATA KULIAH ----//

            // ---- START PROFILE ----//
            case 'profile':
              include 'profile/profile.php';
              break;
            // manage Profile
            case 'profile-update':
              include 'profile/manage-profile/profile-update.php';
              break;
            case 'password-update':
              include 'profile/manage-profile/password-update.php';
              break;
            // ---- END PROFILE ----//

            // ---- START PANDUAN ----//
            case 'panduan':
              include 'panduan/panduan-pengisian.php';
              break;
            case 'classroom':
              include 'panduan/panduan-classroom.php';
              break;
            // ---- END PANDUAN ----//

            // --- START KONTAK ---//
            case 'kontak':
              include 'kontak/kontak.php';
              break;
            // --- END KONTAK ---//

            default:
              include "not-found.php";
              break;
          }
        }else{
          include "profile/profile.php";
        }
        //----------END ADMIN PAGE----------//

     ?>


  </div>
  <!-- /.content-wrapper -->
  <?php include '../../struktur/footer.php'; ?>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<?php include '../../struktur/jquery.php'; ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-162978976-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-162978976-6');
</script>
</body>


</html>
