<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <title></title>
</head>
<body>
  <section class="content-header">
    <h1>
      Layanan Informasi & Pengaduan
      <!-- <small>Dosen</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-phone"></i>Informasi & Pengaduan</a></li>
    </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-sipamas">
            <!-- /.box-header -->
            <div class="box-body">
              <p>
              Jika anda mengalami kesulitan menggunakan sistem Monev E-Learning, <br>atau memiliki keluhan terhadap sistem ini dapat menguhubungi kontak Whatsapp dibawah ini.
              <br><br>
              <b>(Cahyo | Pusat IT & PDPT Universitas Widyagama)</b><br>
              <a href="https://api.whatsapp.com/send?phone=6282231916221" target="_blank"><img src="../../file/whatsapp-button.png" width="175"></a>
              </p>

            </div>
          </div>
        </div>
      </div>
  </section>
</body>
</html>
