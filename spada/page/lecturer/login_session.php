<?php
session_start();
if(!isset($_SESSION['username'])){
    header("Location: ../../login/signin.php?msg=error1");
} elseif($_SESSION['role']!="dosen"){
    header("Location: ../../login/signin.php?msg=error2");
}
?>
