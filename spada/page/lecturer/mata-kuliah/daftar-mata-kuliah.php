<?php
include "../../../../sso-uwg/koneksi/koneksi.php";

function getWeeks($date, $rollover)
    {
        $cut = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first = strtotime($cut . "00");
        $elapsed = ($timestamp - $first) / $daylen;

        $weeks = 1;

        for ($i = 1; $i <= $elapsed; $i++)
        {
            $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if($day == strtolower($rollover))  $weeks ++;
        }

        return $weeks;
    }

?>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <title></title>
  <style type="text/css">
  .fa-input {
    font-family: FontAwesome, 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  </style>
</head>
<body>
  <section class="content-header">
    <h1>
      Daftar Mata Kuliah
      <!-- <small>Kegiatan Penelitian</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-file-o"></i>Daftar Mata Kuliah</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="alert alert-warning alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" data-toggle="tooltip" data-placement="left" title="" aria-hidden="true" data-original-title="Tutup">×</button>
          <h4><i class="icon fa fa-bullhorn"></i> Pengumuman!</h4>
            <p>
            
              [1] Semester aktif 2020/2021 Genap.<br>
    
            </p>
          </div>
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" data-toggle="tooltip" data-placement="left" title="Tutup" aria-hidden="true">×</button>
          <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
          Sebelum anda menambahkan mata kuliah baru mohon perhatikan: <br><b>[1]</b> Kode MK harus benar dan sesuai dengan kelas, tanpa menambahkan prefix kurikulum (K15 / K18 / K20). <br><b>[2]</b> Buatlah kelas pada <i>Google Classroom</i> terlebih dahulu sebelum mengisi monev. <a href="index.php?page=classroom">Panduan Google Classroom</a>.<br><br>Kode MK & Kode <i>Google Classroom</i> <b>tidak dapat diubah</b>.
        </div>
        <div class="box box-sipamas">
          <!-- <div class="box-header">
          <h3 class="box-title">Data Hari Kerja Kepegawaian Universitas Widyagama Malang</h3>
        </div> -->

        <!-- /.box-header -->
        <div class="box-body">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-condensed table-bordered table-hover" id="tabel-data">
                <thead>
                  <tr class="text-center">
                    <th class="text-center" style="width: 10px">No</th>
                    <th class="text-center" style="width: 80px">Kode MK</th>
                    <th class="text-center">Nama Mata Kuliah</th>
                    <th class="text-center" style="width: 160px">Fakultas / Program Studi</th>
                    <th class="text-center" style="width: 20px">Jumlah Pertemuan</th>
                    <th class="text-center" style="width: 75px">Aksi</th>
                  </tr>
                </thead>
                <?php
                $no = 1;
                $sql = "SELECT * FROM tbl_spada_mk WHERE nidn = '$_SESSION[username]'";
                $data = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
                while ($row = mysqli_fetch_array($data)) {
                  ?>
                  <tr>
                    <td class="text-center"><?php echo $no++; ?></td>
                    <td class="text-center"><?php echo $row['kode_mk']; ?></td>
                    <td><b><?php echo $row['nama_mk']; ?></b><br><br>
                      Kode <i>Google Classroom</i>: <span class='badge bg-secondary'> <?php echo $row['kode_classroom']; ?></span>
                    </td>
                    <td class="text-center"><?php echo $row['fak'] . ' / ' . $row['jur']; ?></td>
                    <td class="text-center">
                      <?php
                      $sql2 = "SELECT COUNT(kode_mk) AS num FROM tbl_spada_pertemuan WHERE kode_mk = '$row[kode_mk]' && nidn = '$_SESSION[username]'";
                      $data2 = mysqli_query($koneksi, $sql2) or die(mysqli_error($koneksi));
                      $row2 = mysqli_fetch_array($data2);
                      echo $row2['num'];
                      ?>
                    </td>

                    <td class="text-center">
                      <a href="index.php?page=pertemuan&kode_mk=<?php echo $row['kode_mk']; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" title="Pertemuan" id="<?php echo $row['id_pe']; ?>"><i class="fa fa-info-circle"></i> </a>

                      <span data-toggle="modal" data-target="#myModal<?php echo $row['id_spada_mk']; ?>">
                        <a href="#" type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Edit Mata Kuliah" ><i class="fa fa-pencil"></i></a>
                      </span>

                      <?php
                      echo "<a onClick='confirm_delete(\"index.php?page=mk-delete&kode_mk=$row[kode_mk]\")' class='btn btn-xs btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus Mata Kuliah'><i class='fa fa-trash'></i> </a>"; ?>
                    </td>
                  </tr>

                  <!-- start modal tombol edit -->
                  <div class="modal fade" id="myModal<?php echo $row['id_spada_mk']; ?>" role="dialog">
                    <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header header-sipamas">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" align="center"><font color="white"><h4>
                            <b>Edit Mata Kuliah - <?php echo $row['kode_mk']; ?></b>
                          </h4></font></h4>
                        </div>
                        <div class="modal-body">
                          <p class="text-center"><font color="red"><b>Perhatian!</b> Pastikan kembali data yang diisi adalah benar dan lengkap.</font></p><br>
                          <form id="update_form" role="form" action="index.php?page=mk-update" method="post" class="registration-form">
                            <?php
                            $idKode = $row['kode_mk'];
                            $query_edit = mysqli_query($koneksi, "SELECT * FROM tbl_spada_mk WHERE kode_mk='$idKode'");
                            //$result = mysqli_query($conn, $query);
                            while ($data_edit = mysqli_fetch_array($query_edit)) {
                              // print_r($data_edit);
                              ?>
                              <input style="width:536px" name="id_spada_mk" type="hidden" class="form-control text-center" value="<?php echo $data_edit["id_spada_mk"]; ?>" readonly/>

                              <input style="width:536px" name="kode_mk" type="hidden" class="form-control text-center" value="<?php echo $data_edit["kode_mk"]; ?>" readonly/>

                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Kode Mata Kuliah*</label>
                                  <div class="input-group">
                                    <input style="width:250px" name="kode_mk" type="text" class="form-control" value="<?php echo $data_edit['kode_mk'];?>" disabled/>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label>Fakultas*</label>
                                  <div class="input-group">
                                    <?php $fakultas = $data_edit['fak']; ?>
                                    <select class="form-control" style="width: 250px;" name="fakultas" id="fakultas" required>
                                      <option <?php echo ($fakultas == 'Fakultas Ekonomi dan Bisnis') ? "selected": "" ?>>Fakultas Ekonomi dan Bisnis</option>
                                      <option <?php echo ($fakultas == 'Fakultas Hukum') ? "selected": "" ?>>Fakultas Hukum</option>
                                      <option <?php echo ($fakultas == 'Fakultas Pertanian') ? "selected": "" ?>>Fakultas Pertanian</option>
                                      <option <?php echo ($fakultas == 'Fakultas Teknik') ? "selected": "" ?>>Fakultas Teknik</option>
                                      <option <?php echo ($fakultas == 'Pascasarjana') ? "selected": "" ?>>Pascasarjana</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Kode <i>Google Classroom</i>*</label>
                                  <div class="input-group">
                                    <input style="width:250px" name="kode_classroom" type="text" class="form-control" value="<?php echo $data_edit['kode_classroom']; ?>" disabled/>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label>Jurusan*</label>
                                  <div class="input-group">
                                    <?php $jurusan = $data_edit['jur'];?>
                                    <select class="form-control" style="width: 250px;" name="jurusan" id="jurusan" required>
                                      <option <?php echo ($jurusan == 'D3 Keuangan & Perbankan') ? "selected": "" ?>>D3 Keuangan & Perbankan</option>
                                      <option <?php echo ($jurusan == 'D3 Otomotif') ? "selected": "" ?>>D3 Otomotif</option>
                                      <option disabled>----</option>
                                      <option <?php echo ($jurusan == 'S1 Akuntansi') ? "selected": "" ?>>S1 Akuntansi</option>
                                      <option <?php echo ($jurusan == 'S1 Manajemen') ? "selected": "" ?>>S1 Manajemen</option>
                                      <option <?php echo ($jurusan == 'S1 Ilmu Hukum') ? "selected": "" ?>>S1 Ilmu Hukum</option>
                                      <option <?php echo ($jurusan == 'S1 Agrobisnis') ? "selected": "" ?>>S1 Agrobisnis</option>
                                      <option <?php echo ($jurusan == 'S1 Agroteknologi') ? "selected": "" ?>>S1 Agroteknologi</option>
                                      <option <?php echo ($jurusan == 'S1 Teknologi Hasil Pertanian') ? "selected": "" ?>>S1 Teknologi Hasil Pertanian</option>
                                      <option <?php echo ($jurusan == 'S1 Teknik Elektro') ? "selected": "" ?>>S1 Teknik Elektro</option>
                                      <option <?php echo ($jurusan == 'S1 Teknik Informatika') ? "selected": "" ?>>S1 Teknik Informatika</option>
                                      <option <?php echo ($jurusan == 'S1 Teknik Mesin') ? "selected": "" ?>>S1 Teknik Mesin</option>
                                      <option <?php echo ($jurusan == 'S1 Teknik Industri') ? "selected": "" ?>>S1 Teknik Industri</option>
                                      <option <?php echo ($jurusan == 'S1 Teknik Sipil') ? "selected": "" ?>>S1 Teknik Sipil</option>
                                      <option disabled>----</option>
                                      <option <?php echo ($jurusan == 'S2 Manajemen') ? "selected": "" ?>>S2 Manajemen</option>
                                      <option <?php echo ($jurusan == 'S2 Hukum') ? "selected": "" ?>>S2 Hukum</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>Nama Mata Kuliah*</label>
                                  <div class="input-group">
                                    <textarea style="width:536px" name="nama_mk" class="form-control" required><?php echo $data_edit["nama_mk"]; ?></textarea>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-12">
                                <hr>
                              </div>
                              <?php
                            }
                            //mysql_close($host);
                            ?>
                              <!-- penutup while $data_edit -->
                            </div>
                            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                              <button type="reset" class="btn btn-sm btn-primary" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-left"></i> Kembali</button>
                              <!-- <button type="submit" id="submit" form="update_form" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button> -->
                              <input type="submit" name="update" value="&#xf0c7; Simpan" class="btn btn-sm btn-success fa-input" />
                            </div>
                            </div>
                          </form>

                      </div>
                    </div>
                    <!-- end modal tombol edit -->
                    <?php
                  }
                  ?>
                </table>
                <div class="text-left">
                  <button type="button" name="tambah" id="tambah" data-toggle="tooltip" data-target="#ModalAdd" class="btn btn-sm btn-primary open_modal_add"><i class="fa fa-plus-circle"></i> Tambah Data </button>
                  <?php
                  echo "<a onClick='truncate(\"index.php?page=mk-truncate\")' class='btn btn-sm btn-danger'><i class='fa fa-times'></i> Kosongkan Tabel</a>";
                  ?>
                </div>

              </div>
            </div>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

</body>
</html>

<!-- Modal Popup Add -->
<div id="ModalAdd" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>


<!-- Modal Popup Delete-->
<div id="ModalDelete" class="modal modal-warning fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="text-align:center;">Apakah anda yakin menghapus data ini ?</h4>
      </div>
      <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
        <button type="button" class="btn btn-sm btn-outline" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Kembali</button>
        <a href="#" class="btn btn-sm btn-outline" id="delete_link"><i class="fa fa-trash"></i> Hapus</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal Popup Truncate-->
<div id="ModalTruncate" class="modal modal-danger fade" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="text-align:center;">Apakah anda yakin mengosongkan tabel ini ?</h4>
      </div>
      <div class="modal-body">
        <p align="center">Anda akan menghapus SELURUH <b>Mata Kuliah beserta Pertemuannya</b>. <br>Peringatan! Data yang telah dihapus tidak dapat dikembalikan.</p>
      </div>
      <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
        <button type="button" class="btn btn-sm btn-outline" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Kembali</button>
        <a href="#" class="btn btn-sm btn-outline" id="truncate_link"><i class="fa fa-times"></i> Kosongkan Tabel</a>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  var table = $('#tabel-data').DataTable({
    'info'  : true,
    'ordering' : false,
    'pageLength' : 5,
    dom: 'lBfrtip',
    // buttons: [
    //   'copy', 'csv', 'excel', 'pdf', 'print',
    // ],
    buttons: [
      {
        extend: 'copy',
        text: '<u>C</u>opy',
        exportOptions: {
          columns: ':visible:not(:eq(5))'
        },
        key: {
          key: 'c',
          ctrlkey: true
        }
      },
      {
        extend: 'csv',
        title: 'DAFTAR MATA KULIAH MONEV E-LEARNING',
        exportOptions: {
          columns: ':visible:not(:eq(5))'
        }
      },
      {
        extend: 'excel',
        title: 'DAFTAR MATA KULIAH MONEV E-LEARNING',
        pageSize: 'A4',
        exportOptions: {
          columns: ':visible:not(:eq(5))'
        }
      },
      {
        extend: 'pdf',
        title: 'DAFTAR MATA KULIAH MONEV E-LEARNING',
        pageSize: 'A4',
        orientation: 'landscape',
        exportOptions: {
          columns: ':visible:not(:eq(5))'
        }
      },
      {
        extend: 'print',
        text: '<u>P</u>rint',
        pageSize: 'A4',
        orientation: 'landscape',
        exportOptions: {
          columns: ':visible:not(:eq(5))'
        },
        key: {
          key: 'p',
          ctrlkey: true
        },
        customize: function (win) {
          $(win.document.body).find('table').addClass('display').css('font-size', '12px');
          $(win.document.body).find('tr:nth-child(odd) td').each(function(index){
            $(this).css('background-color','#D0D0D0');
          });
          $(win.document.body).find('h1').css('text-align','center');
        }
      },
    ],
    fixedHeader: {
      header: true,
      footer: true
    },
    'language' : {'emptyTable'  : 'Tidak ada data yang tersedia pada tabel ini',
    'info'        : 'Menampilkan _START_ hingga _END_ dari _TOTAL_ data',
    'infoEmpty'   : 'Menampilkan 0 hingga 0 dari 0 data',
    'infoFiltered': '(dicari dari total _MAX_ data)',
    'search'      : 'Pencarian data:',
    'lengthMenu'  : 'Tampilkan _MENU_ data',
    'zeroRecords' : 'Tidak ditemukan data yang cocok',
    'paginate'    : {
      'first'   : 'Pertama',
      'last'    : 'Terakhir',
      'next'    : 'Selanjutnya',
      'previous': 'Sebelumnya'
    },
  },
});
});
</script>

<!-- Modal Popup Edit -->
<!-- <div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"></div> -->

<!-- Javascript Edit-->
<!-- <script type="text/javascript">
$(document).ready(function () {

// edit
$(".open_modal").click(function(e) {
var m = $(this).attr("id");
$.ajax({
url: "mata-kuliah/modal-edit-penelitian.php",
type: "GET",
data : {id_pe: m,},
success: function (ajaxData){
$("#ModalEdit").html(ajaxData);
$("#ModalEdit").modal('show',{backdrop: 'true'});
}
});
});
});
</script> -->

<!-- Javascript Add-->
<script>
$(document).ready(function () {

  $(".open_modal_add").click(function(add) {
    $.ajax({
      url: "mata-kuliah/modal-mata-kuliah/modal-mk-add.php",
      success: function(ajaxData) {
        $("#ModalAdd").html(ajaxData);
        $("#ModalAdd").modal('show',{backdrop: 'true'});
      }
    });
  });
});
</script>

<!-- Javascript Delete -->
<script>
function confirm_delete(delete_url){
  $("#ModalDelete").modal('show', {backdrop: 'static'});
  document.getElementById('delete_link').setAttribute('href', delete_url);
}
</script>

<!-- Javascript Truncate -->
<script>
function truncate(delete_url){
  $("#ModalTruncate").modal('show', {backdrop: 'static'});
  document.getElementById('truncate_link').setAttribute('href', delete_url);
}
</script>
