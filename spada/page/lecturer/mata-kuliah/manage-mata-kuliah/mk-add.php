<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../../../assets/plugins/Sweetalert/dist/sweetalert.css">
	<script type="text/javascript" src="../../../../assets/plugins/Sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>

	<?php

	include "../../../koneksi/koneksi.php";
	include "../../spada/page/lecturer/mata-kuliah/daftar-mata-kuliah.php";

	if ($_POST['SIMPAN']) {
		$file_rps = 0;
		$nidn					= $_SESSION['username'];
		$kode_mk1   	  		= mysqli_real_escape_string($koneksi, strtoupper($_POST['kode_mk']));
		$kode_mk				= preg_replace('/[^A-Za-z0-9\- ]/', '', $kode_mk1);
		$nama_mk		  		= mysqli_real_escape_string($koneksi, strtoupper($_POST['nama_mk']));
		$kode_classroom	 	 	= mysqli_real_escape_string($koneksi, $_POST['kode_classroom']);
		$fakultas	     		= mysqli_real_escape_string($koneksi, $_POST['fakultas']);
		$jurusan	     		= mysqli_real_escape_string($koneksi, $_POST['jurusan']);

		$modalquery				= mysqli_query($koneksi, "INSERT INTO tbl_spada_mk VALUES (NULL, '$kode_mk', '$nidn', '$nama_mk', '$kode_classroom', '$fakultas', '$jurusan','$file_rps')");
		// echo "<pre>";
		// print_r($modalquery);
		// echo "</pre>";
		if ($modalquery) {
			echo "
			<script>
			swal({
				title: 'Sukses',
				text: 'Data berhasil disimpan',
				type: 'success',
				showCancelButton: false,
				cancelButtonText: 'No, Cancel!',
				confirmButtonText: 'Oke',
				closeOnConfirm: true
			}, function(isConfirm){
				window.location='index.php?page=mata-kuliah'
			});
			</script>";
			// print_r($modalquery);
		} else {
			// echo "gagal";
			echo "
			<script>
			swal({
				title: 'Error',
				text: 'Data gagal disimpan',
				type: 'error',
				showCancelButton: false,
				cancelButtonText: 'No, Cancel!',
				confirmButtonText: 'Oke',
				closeOnConfirm: true
			}, function(isConfirm){
				window.location='index.php?page=mata-kuliah'
			});
			</script>"; //*/
		}
	}
	?>

</body>
</html>
