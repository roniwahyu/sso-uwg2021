<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../../../assets/plugins/Sweetalert/dist/sweetalert.css">
	<script type="text/javascript" src="../../../../assets/plugins/Sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>

	<?php
	include "../../../koneksi/koneksi.php";
	include "../../spada/page/lecturer/mata-kuliah/daftar-mata-kuliah.php";
	
	$nidn			= $_SESSION['username'];

	$query1 = mysqli_query($koneksi, "DELETE FROM tbl_spada_mk WHERE nidn='$nidn'");
	$query2 = mysqli_query($koneksi, "DELETE FROM tbl_spada_pertemuan WHERE nidn='$nidn'");

	if (($query1) && ($query2))
	{
		echo "
		<script>
		swal({
			title: 'Sukses',
			text: 'Tabel berhasil dikosongkan',
			type: 'success',
			showCancelButton: false,
			cancelButtonText: 'No, Cancel!',
			confirmButtonText: 'Oke',
			closeOnConfirm: true
		}, function(isConfirm){
			window.location='index.php?page=mata-kuliah'
		});
		</script>";

	}
	else
	{
		echo "
		<script>
		swal({
			title: 'Error',
			text: 'Tabel gagal dikosongkan',
			type: 'error',
			showCancelButton: false,
			cancelButtonText: 'No, Cancel!',
			confirmButtonText: 'Oke',
			closeOnConfirm: true
		}, function(isConfirm){
			window.location='index.php?page=mata-kuliah'
		});
		</script>";
	}
	?>


</body>
</html>
