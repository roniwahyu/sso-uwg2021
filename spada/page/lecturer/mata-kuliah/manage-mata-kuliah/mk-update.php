<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../../../assets/plugins/Sweetalert/dist/sweetalert.css">
	<script type="text/javascript" src="../../../../assets/plugins/Sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>

	<?php
	include "../../../koneksi/koneksi.php";
	include "../../spada/page/lecturer/mata-kuliah/daftar-mata-kuliah.php";

	$id_spada_mk   	= $_POST['id_spada_mk'];
	$kode_mk   	 	 	= $_POST['kode_mk'];
	$nama_mk		    = mysqli_real_escape_string($koneksi, strtoupper($_POST['nama_mk']));
	$fakultas	     	= mysqli_real_escape_string($koneksi, $_POST['fakultas']);
	$jurusan	     	= mysqli_real_escape_string($koneksi, $_POST['jurusan']);

	$modalquery = mysqli_query($koneksi, "UPDATE tbl_spada_mk SET nama_mk='$nama_mk', fak='$fakultas', jur='$jurusan' WHERE id_spada_mk='$id_spada_mk'");

	if ($modalquery == TRUE)
	{
		echo "
		<script>
		swal({
			title: 'Sukses',
			text: 'Data berhasil disimpan',
			type: 'success',
			showCancelButton: false,
			cancelButtonText: 'No, Cancel!',
			confirmButtonText: 'Oke',
			closeOnConfirm: true
		}, function(isConfirm){
			window.location='index.php?page=pertemuan&kode_mk=$kode_mk'
		});
		</script>";

	}
	else
	{
		echo "
		<script>
		swal({
			title: 'Error',
			text: 'Data gagal disimpan',
			type: 'error',
			showCancelButton: false,
			cancelButtonText: 'No, Cancel!',
			confirmButtonText: 'Oke',
			closeOnConfirm: true
		}, function(isConfirm){
			window.location='index.php?page=pertemuan&kode_mk=$kode_mk'
		});
		</script>";
	}
	?>

</body>
</html>
