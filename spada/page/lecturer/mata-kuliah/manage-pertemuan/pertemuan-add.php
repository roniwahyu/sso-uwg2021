<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../../../assets/plugins/Sweetalert/dist/sweetalert.css">
	<script type="text/javascript" src="../../../../assets/plugins/Sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>

	<?php

	include "../../../koneksi/koneksi.php";
	include "../../spada/page/lecturer/mata-kuliah/pertemuan-mata-kuliah.php";

	if ($_POST['SIMPAN']) {

		$kode_mk   				= $_POST['kode_mk'];
		$nidn							= $_SESSION['username'];
		$materi	 	 				= mysqli_real_escape_string($koneksi, $_POST['materi']);
		$tugas	 	 				= mysqli_real_escape_string($koneksi, $_POST['tugas']);
		$tanggal					= mysqli_real_escape_string($koneksi, $_POST['tanggal']);
		$mhs_turn_in	 	 	= mysqli_real_escape_string($koneksi, $_POST['mhs_turn_in']);

		$query = "SELECT max(id) as maxid FROM tbl_spada_pertemuan WHERE kode_mk='$kode_mk'";

		$hasil = mysqli_query($koneksi, $query);
		$data  = mysqli_fetch_array($hasil);
		$kodeid = $data['maxid'];

		$kodeid = (int)substr($kodeid, -2);
		$kodeidNew = $kodeid + 1;
		if ($kodeidNew >= 1 && $kodeidNew <= 9) {
			$kodeidNew1 = '00'. $kodeidNew;
		} elseif ($kodeidNew >= 9 && $kodeidNew <= 99) {
			$kodeidNew1 = '0'. $kodeidNew;
		} elseif ($kodeidNew >= 99 && $kodeidNew <= 999) {
			$kodeidNew1 = $kodeidNew;
		} else {
			$kodeidNew1 = '0001';
		}

		$query2 	= "SELECT kode_classroom FROM tbl_spada_mk WHERE kode_mk='$kode_mk'";
		$hasil2 	= mysqli_query($koneksi, $query2);
		$data2		= mysqli_fetch_array($hasil2);

		$id = $data2['kode_classroom'] . $kodeidNew1;


		$modalquery				= mysqli_query($koneksi, "INSERT INTO tbl_spada_pertemuan VALUES ('$id', '$nidn', '$kode_mk', '$materi', '$tugas', '$mhs_turn_in', '$tanggal')");

		// echo $id . $nidn . $kode_mk . $materi . $tugas . $mhs_turn_in . $tanggal	;
		if ($modalquery == TRUE) {
			echo "
			<script>
			swal({
				title: 'Sukses',
				text: 'Data berhasil disimpan',
				type: 'success',
				showCancelButton: false,
				cancelButtonText: 'No, Cancel!',
				confirmButtonText: 'Oke',
				closeOnConfirm: true
			}, function(isConfirm){
				window.location='index.php?page=pertemuan&kode_mk=$kode_mk'
			});
			</script>";
		} else {
			echo "
			<script>
			swal({
				title: 'Error',
				text: 'Data gagal disimpan',
				type: 'error',
				showCancelButton: false,
				cancelButtonText: 'No, Cancel!',
				confirmButtonText: 'Oke',
				closeOnConfirm: true
			}, function(isConfirm){
				window.location='index.php?page=pertemuan&kode_mk=$kode_mk'
			});
			</script>";
		}
	}
	?>

</body>
</html>
