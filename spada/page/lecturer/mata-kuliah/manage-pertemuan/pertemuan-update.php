<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../../../assets/plugins/Sweetalert/dist/sweetalert.css">
	<script type="text/javascript" src="../../../../assets/plugins/Sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>

	<?php
	include "../../../koneksi/koneksi.php";
	include "../../spada/page/lecturer/mata-kuliah/pertemuan-mata-kuliah.php";

	$id   	     			= $_POST['id'];
	$kode_mk   	     	= $_POST['kode_mk'];
	$nidn							= $_SESSION['username'];
	$materi	 	 				= mysqli_real_escape_string($koneksi, $_POST['materi']);
	$tugas	 	 				= mysqli_real_escape_string($koneksi, $_POST['tugas']);
	$tanggal					= mysqli_real_escape_string($koneksi, $_POST['tanggal']);
	$mhs_turn_in	 	 	= mysqli_real_escape_string($koneksi, $_POST['mhs_turn_in']);

	$modalquery = mysqli_query($koneksi, "UPDATE tbl_spada_pertemuan SET materi='$materi', tugas='$tugas', tanggal='$tanggal', mhs_turn_in='$mhs_turn_in' WHERE id='$id' && nidn='$nidn'");

	if ($modalquery)
	{
		echo "
		<script>
		swal({
			title: 'Sukses',
			text: 'Data berhasil disimpan',
			type: 'success',
			showCancelButton: false,
			cancelButtonText: 'No, Cancel!',
			confirmButtonText: 'Oke',
			closeOnConfirm: true
		}, function(isConfirm){
			window.location='index.php?page=pertemuan&kode_mk=$kode_mk'
		});
		</script>";

	}
	else
	{
		echo "
		<script>
		swal({
			title: 'Error',
			text: 'Data gagal disimpan',
			type: 'error',
			showCancelButton: false,
			cancelButtonText: 'No, Cancel!',
			confirmButtonText: 'Oke',
			closeOnConfirm: true
		}, function(isConfirm){
			window.location='index.php?page=pertemuan&kode_mk=$kode_mk'
		});
		</script>";
	}
	?>

</body>
</html>
