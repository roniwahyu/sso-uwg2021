<html>
<head>
  <title></title>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
  <!-- InputMask -->
  <script src="../../../../assets/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="../../../../assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="../../../../assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
</head>
<body>
<div class="modal-dialog modal-dialog-centered">
  <div class="modal-content">
    <div class="modal-header header-sipamas">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" align="center"><font color="white"><h4>
        <b>Tambah Data - Mata Kuliah</b>
      </h4></font></h4>
    </div>
    <div class="modal-body">
      <p class="text-center"><font color="red"><b>Perhatian!</b> Pastikan kembali data yang diisi adalah benar dan lengkap.</font></p><br>
      <form id="add_form" action="index.php?page=mk-add" method="post">
        <div class="col-md-6">
          <div class="form-group">
            <label>Kode Mata Kuliah (Tanpa Prefix K20)*</label>
            <div class="input-group">
              <input style="width:250px" name="kode_mk" id="input_mask" type="text" class="form-control" required/>
              <small class="form-text text-muted"><i>Tambah kode A atau B setelah kode MK untuk membedakan kelas Reg A atau Reg B. <br><b>Contoh: IFT-100-A, FTK-402-A2</b></i></small>
            </div>
          </div>
          <div class="form-group">
            <label>Fakultas*</label>
            <div class="input-group">
              <select class="form-control" style="width: 250px;" name="fakultas" id="fakultas" required>
                <option value="">--- Pilih Fakultas ---</option>
                <option value="Fakultas Ekonomi dan Bisnis">Fakultas Ekonomi dan Bisnis</option>
                <option value="Fakultas Hukum">Fakultas Hukum</option>
                <option value="Fakultas Pertanian">Fakultas Pertanian</option>
                <option value="Fakultas Teknik">Fakultas Teknik</option>
                <option value="Pascasarjana">Pascasarjana</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Kode <i>Google Classroom</i>*</label>
            <div class="input-group">
              <input style="width:250px" name="kode_classroom" type="text" class="form-control" value="" required/>
              <small class="form-text text-muted"><i>Hanya tulis Kode Classroom bukan link (URL) Classroom. <b><br>Contoh: 4j5yrp</b></i></small>
            </div>
          </div>
          <div class="form-group">
            <label>Program Studi*</label>
            <div class="input-group">
              <select class="form-control" style="width: 250px;" name="jurusan" id="jurusan">
                <option value="">--- Pilih Program Studi ---</option>
                <option value="D3 Keuangan & Perbankan Syariah">D3 Keuangan & Perbankan Syariah</option>
                <option value="D3 Otomotif">D3 Otomotif</option>
                <option disabled>----</option>
                <option value="S1 Akuntansi">S1 Akuntansi</option>
                <option value="S1 Manajemen">S1 Manajemen</option>
                <option value="S1 Ilmu Hukum">S1 Ilmu Hukum</option>
                <option value="S1 Agrobisnis">S1 Agrobisnis</option>
                <option value="S1 Agroteknologi">S1 Agroteknologi</option>
                <option value="S1 Teknologi Hasil Pertanian">S1 Teknologi Hasil Pertanian</option>
                <option value="S1 Teknik Elektro">S1 Teknik Elektro</option>
                <option value="S1 Teknik Informatika">S1 Teknik Informatika</option>
                <option value="S1 Teknik Mesin">S1 Teknik Mesin</option>
                <option value="S1 Teknik Industri">S1 Teknik Industri</option>
                <option value="S1 Teknik Sipil">S1 Teknik Sipil</option>
                <option disabled>----</option>
                <option value="S2 Manajemen">S2 Manajemen</option>
                <option value="S2 Hukum">S2 Hukum</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Nama Mata Kuliah*</label>
            <div class="input-group">
              <textarea style="width:536px" name="nama_mk" class="form-control" required></textarea>
              <small class="form-text text-muted"><i>Tambah kode A atau B setelah nama MK untuk membedakan kelas Reg A atau Reg B. <br><b>Contoh: Data Mining-A</b></i></small class="form-text text-muted">
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
      </form>
      <br>
    </div>
    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
      <button type="reset" class="btn btn-sm btn-primary" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-left"></i> Kembali</button>
      <button form=add_form class="btn btn-sm btn-success" id="SIMPAN" value="SIMPAN" name="SIMPAN" type="submit"><i class="fa fa-save"></i> Simpan</button>
    </div>
  </div>
</div>


</body>
</html>

<script>
  jQuery(function($){
    $('#input_mask').inputmask({
        mask: 'AAA-AAA-AAAAAA',
        definitions: {
            A: {
                validator: "[A-Za-z0-9 ]"
            },
        },
    });
  });
</script>
