<?php
include "../../../../../../sso-uwg/koneksi/koneksi.php";
$idKode = $_GET['kode_mk'];

$query = mysqli_query($koneksi, "SELECT * FROM tbl_spada_mk WHERE kode_mk='$idKode'");
if($query == false){
  die ("Terjadi Kesalahan : ". mysqli_error($koneksi));
}
while($data_edit = mysqli_fetch_array($query)){
  ?>
  <style type="text/css">
  .fa-input {
    font-family: FontAwesome, 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  </style>

  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header header-sipamas">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" align="center"><font color="white"><h4>
          <b>Edit Mata Kuliah - <?php echo $data_edit["kode_mk"]; ?></b>
        </h4></font></h4>
      </div>

      <div class="modal-body">
        <p class="text-center"><font color="red"><b>Perhatian!</b> Pastikan kembali data yang diisi adalah benar dan lengkap.</font></p><br>

        <form id="update_mk_form" role="form" action="index.php?page=mk-update" method="post" class="registration-form">

          <input style="width:536px" name="id_spada_mk" type="hidden" class="form-control text-center" value="<?php echo $data_edit["id_spada_mk"]; ?>" readonly/>

          <input style="width:536px" name="kode_mk" type="hidden" class="form-control text-center" value="<?php echo $data_edit["kode_mk"]; ?>" readonly/>

          <div class="col-md-6">
            <div class="form-group">
              <label>Kode Mata Kuliah (Tanpa Prefix K20)*</label>
              <div class="input-group">
                <input style="width:250px" name="kode_mk" type="text" class="form-control" value="<?php echo $data_edit["kode_mk"]; ?>" disabled/>
              </div>
            </div>
            <div class="form-group">
              <label>Fakultas*</label>
              <div class="input-group">
                <?php $fakultas = $data_edit['fak']; ?>
                <select class="form-control" style="width: 250px;" name="fakultas" id="fakultas" required>
                  <option <?php echo ($fakultas == 'Fakultas Ekonomi dan Bisnis') ? "selected": "" ?>>Fakultas Ekonomi dan Bisnis</option>
                  <option <?php echo ($fakultas == 'Fakultas Hukum') ? "selected": "" ?>>Fakultas Hukum</option>
                  <option <?php echo ($fakultas == 'Fakultas Pertanian') ? "selected": "" ?>>Fakultas Pertanian</option>
                  <option <?php echo ($fakultas == 'Fakultas Teknik') ? "selected": "" ?>>Fakultas Teknik</option>
                  <option <?php echo ($fakultas == 'Pascasarjana') ? "selected": "" ?>>Pascasarjana</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Kode <i>Google Classroom</i>*</label>
              <div class="input-group">
                <input style="width:250px" name="kode_classroom" type="text" class="form-control" value="<?php echo $data_edit["kode_classroom"]; ?>" disabled/>
              </div>
            </div>
            <div class="form-group">
              <label>Program Studi*</label>
              <div class="input-group">
                <?php $jurusan = $data_edit['jur'];?>
                <select class="form-control" style="width: 250px;" name="jurusan" id="jurusan" required>
                  <option <?php echo ($jurusan == 'D3 Keuangan & Perbankan') ? "selected": "" ?>>D3 Keuangan & Perbankan</option>
                  <option <?php echo ($jurusan == 'D3 Otomotif') ? "selected": "" ?>>D3 Otomotif</option>
                  <option disabled>----</option>
                  <option <?php echo ($jurusan == 'S1 Akuntansi') ? "selected": "" ?>>S1 Akuntansi</option>
                  <option <?php echo ($jurusan == 'S1 Manajemen') ? "selected": "" ?>>S1 Manajemen</option>
                  <option <?php echo ($jurusan == 'S1 Ilmu Hukum') ? "selected": "" ?>>S1 Ilmu Hukum</option>
                  <option <?php echo ($jurusan == 'S1 Agrobisnis') ? "selected": "" ?>>S1 Agrobisnis</option>
                  <option <?php echo ($jurusan == 'S1 Agroteknologi') ? "selected": "" ?>>S1 Agroteknologi</option>
                  <option <?php echo ($jurusan == 'S1 Teknologi Hasil Pertanian') ? "selected": "" ?>>S1 Teknologi Hasil Pertanian</option>
                  <option <?php echo ($jurusan == 'S1 Teknik Elektro') ? "selected": "" ?>>S1 Teknik Elektro</option>
                  <option <?php echo ($jurusan == 'S1 Teknik Informatika') ? "selected": "" ?>>S1 Teknik Informatika</option>
                  <option <?php echo ($jurusan == 'S1 Teknik Mesin') ? "selected": "" ?>>S1 Teknik Mesin</option>
                  <option <?php echo ($jurusan == 'S1 Teknik Industri') ? "selected": "" ?>>S1 Teknik Industri</option>
                  <option <?php echo ($jurusan == 'S1 Teknik Sipil') ? "selected": "" ?>>S1 Teknik Sipil</option>
                  <option disabled>----</option>
                  <option <?php echo ($jurusan == 'S2 Manajemen') ? "selected": "" ?>>S2 Manajemen</option>
                  <option <?php echo ($jurusan == 'S2 Hukum') ? "selected": "" ?>>S2 Hukum</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Nama Mata Kuliah*</label>
              <div class="input-group">
                <textarea style="width:536px" name="nama_mk" class="form-control" required><?php echo $data_edit["nama_mk"]; ?></textarea>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <hr>
          </div>
        </form>
        <br>
      </div>
      <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
        <button type="reset" class="btn btn-sm btn-primary" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-left"></i> Kembali</button>
        <button form=update_mk_form class="btn btn-sm btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
        <!-- <input type="submit" name="update" value="&#xf0c7; Simpan" class="btn btn-sm btn-success fa-input" /> -->
      </div>
    </div>
  </div>
  <?php
}
?>
