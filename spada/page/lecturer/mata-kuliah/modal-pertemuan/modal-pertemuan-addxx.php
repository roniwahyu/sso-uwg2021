<?php
include "../../../../../../sso-uwg/koneksi/koneksi.php";
?>

<div class="modal-dialog modal-dialog-centered">
  <div class="modal-content">
    <div class="modal-header header-sipamas">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title text-center" ><font color="white"><h4>
        <b>Tambah Pertemuan Mata Kuliah <?php echo $_GET["kode_mk"];?></b>
      </h4></font></h4>
    </div>
    <div class="modal-body">

      <p class="text-center"><font color="red"><b>Perhatian!</b> Pastikan kembali data yang diisi adalah benar dan lengkap.</font></p><br>
      <form id="add_form" action="index.php?page=pertemuan-add" method="post">

        <input name="kode_mk" type="hidden" value="<?php echo $_GET["kode_mk"]; ?>" readonly/>

        <div class="col-md-12">
          <div class="form-group">
            <label>Materi*</label>
            <div class="input-group">
              <textarea style="width:536px" name="materi" class="form-control" required></textarea>
              <small class="form-text text-muted"><i>Tulis deskripsi singkat materi yang diberikan pada kelas.</i></small class="form-text text-muted">
            </div>
          </div>
          <div class="form-group">
            <label>Tugas</label>
            <div class="input-group">
              <textarea style="width:536px" name="tugas" class="form-control"></textarea>
              <small class="form-text text-muted"><i>Tulis deskripsi singkat tugas yang diberikan kepada mahasiswa.<br>Jika tidak ada tugas, kosongi kolom ini.</i></small class="form-text text-muted">
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Tanggal*</label>
            <div class="input-group">
              <input style="width:250px" name="tanggal" id="daterange" class="form-control" required/>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Mahasiswa Turn In</label>
            <div class="input-group">
              <input style="width:250px" name="mhs_turn_in" type="text" class="form-control" onkeypress="return hanyaAngka(event)"/>
                <small class="form-text text-muted"><i>Jumlah mahasisa yang mengumpulkan tugas.</i></small class="form-text text-muted">
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <hr>
        </div>
      </form>
      <br>
    </div>
    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
      <button type="reset" class="btn btn-sm btn-primary" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-left"></i> Kembali</button>
      <button form=add_form class="btn btn-sm btn-success" id="SIMPAN" value="SIMPAN" name="SIMPAN" type="submit"><i class="fa fa-save"></i> Simpan</button>
    </div>
  </div>
</div>

<script>
		function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))

		    return false;
		  return true;
		}
		
// $('#daterange').daterangepicker({ startDate: '02/01/2021', endDate: '02/14/2021' });
// $('#daterange').datepicker("setDate", new Date(2021,02,01) );
</script>

<script>
// var onlyThisDates = ['1/2/2021', '2/2/2021', '3/2/2021', '4/2/2021', '5/2/2021', '6/2/2021', '7/2/2021', '8/2/2021', '9/2/2021', '10/2/2021', '11/2/2021', '12/2/2021', '13/2/2021', '14/2/2021'];


// $('#daterange').datepicker({
//     format: "dd/mm/yyyy",
//     // startDate: "today",
//     autoclose: true,
//     beforeShowDay: function (date) {
//         var dt_ddmmyyyy = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
//         if (onlyThisDates.indexOf(dt_ddmmyyyy) != -1) {
//             return {
//                 tooltip: 'This date is enabled',
//                 classes: 'active'
//             };
//         } else {
//             return false;
//         }
//     }
// });

$('#daterange').datepicker({ 
    format: "yyyy/mm/dd",
    startDate: '2021/03/25', 
    endDate: '2021/04/05' 
});
</script>
