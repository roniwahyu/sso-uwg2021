<?php
include "../../../../../../sso-uwg/koneksi/koneksi.php";
?>
<!-- Proses Upload File RPS -->

<!-- Akhir Proses Upload File RPS -->
<div class="modal-dialog modal-dialog-centered">
  <div class="modal-content">
    <div class="modal-header header-sipamas">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title text-center" ><font color="white"><h4>
        <b>Tambah File RPS Mata Kuliah <?php echo $_GET["kode_mk"];?></b>
      </h4></font></h4>
    </div>
    <div class="modal-body">
      <?php
        $sql = "SELECT * FROM tbl_spada_mk JOIN users_nama ON tbl_spada_mk.nidn = users_nama.nidn WHERE kode_mk = '$_GET[kode_mk]'";
        $data = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
        while ($row = mysqli_fetch_array($data)) {
      ?>
      <p class="text-center"><font color="red"><b>Perhatian!</b> Pastikan kembali file yang diupload adalah benar dan sesuai.</font></p>
      <!-- <div class="callout callout-warning">
        <h4><i class="icon fa fa-info"></i> Information</h4>

        <p>Fitur ini masih dalam tahap pengembangan.</p>
      </div> -->
      <br>
      <form id="add_form" action="index.php?page=up-rps" method="post" enctype="multipart/form-data">
      <!-- index.php?page=pertemuan-add -->

        <input name="kode_mk" type="hidden" value="<?php echo $_GET["kode_mk"]; ?>" readonly/>

        <div class="col-md-6">
          <div class="form-group">
            <label>Kode Mata Kuliah</label>
            <div class="input-group">
              <input style="width:250px" name="kd_mk" type="text" class="form-control" value="<?php echo $row["kode_mk"]; ?>" readonly/>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Mata Kuliah</label>
            <div class="input-group">
              <input style="width:250px" name="matkul" type="text" class="form-control" value="<?php echo $row["nama_mk"]; ?>" readonly/>
                <!-- <small class="form-text text-muted"><i>Jumlah mahasisa yang mengumpulkan tugas.</i></small class="form-text text-muted"> -->
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label>Upload File RPS*</label>
            <div class="input-group">
              <input style="width:250px" name="file_rps" type="file" class="form-control"/>
              <!-- <small class="form-text text-muted"><i>Tulis deskripsi singkat tugas yang diberikan kepada mahasiswa.<br>Jika tidak ada tugas, kosongi kolom ini.</i></small class="form-text text-muted"> -->
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <hr>
        </div>
        </form>
      <br>
    </div>
    <?php
        }
      ?>
    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
      <button type="reset" class="btn btn-sm btn-primary" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-left"></i> Kembali</button>
      <button form="add_form" class="btn btn-sm btn-success" id="SIMPAN" value="SIMPAN" name="SIMPAN" type="submit"><i class="fa fa-save"></i> Simpan</button>
    </div>
  </div>
</div>

