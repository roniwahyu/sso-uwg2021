 <html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../../../assets/plugins/Sweetalert/dist/sweetalert.css">
	<script type="text/javascript" src="../../../../assets/plugins/Sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>

<?php
include "../../../../../../sso-uwg/koneksi/koneksi.php";

if (isset($_POST['SIMPAN'])) {
  $kd_mk = mysqli_real_escape_string($koneksi, $_POST["kd_mk"]);
  $matkul = mysqli_real_escape_string($koneksi, $_POST["matkul"]);
  $sql1 = mysqli_query($koneksi, "SELECT * FROM tbl_spada_mk WHERE kode_mk = '$kd_mk' AND nama_mk = '$matkul'");
  $cek = mysqli_num_rows($sql1);
    // echo $kd_mk."<br>";
    // echo $matkul."<br><br>";
    // print_r($cek);
    // while ($row = mysqli_fetch_array($sql1)) {
    //     echo $row["kode_mk"]."<br>";
    //     echo $row["nama_mk"];
    // }
  if ($cek > 0) {
    // echo "Query Update";
    $extensi_diperbolehkan = array('pdf');
    $namafile = $_FILES['file_rps']['name'];
    $x = explode('.', $namafile);
    $extensi = strtolower(end($x));
    $ukuran = $_FILES['file_rps']['size'];
    $file_tmp = $_FILES['file_rps']['tmp_name'];

    if(in_array($extensi, $extensi_diperbolehkan) === true){
				if ($ukuran < 50000000) {
          move_uploaded_file($file_tmp, '../../../../sso-uwg/spada/up_rps/'.$namafile);
					$query = "UPDATE tbl_spada_mk SET file_rps = '$namafile' WHERE kode_mk = '$kd_mk' AND nama_mk = '$matkul'";
					if(mysqli_query($koneksi, $query))
					 {
								echo "
									 <script>
									 swal({
										  title: 'Success',
										  text: 'Data berhasil Dimasukkan',
										  type: 'success',
										  showCancelButton: false,
										  cancelButtonText: 'No, Cancel!',
										  confirmButtonText: 'Oke',
										  closeOnConfirm: true
										}, function(isConfirm){
											window.location='index.php?page=pertemuan&kode_mk=$kd_mk'
										});
									</script>";

						}
						else
						{
							echo "
									 <script>
									  swal({
										  title: 'Error',
										  text: 'Gagal Menambahkan Data',
										  type: 'error',
										  showCancelButton: false,
										  cancelButtonText: 'No, Cancel!',
										  confirmButtonText: 'Oke',
										  closeOnConfirm: true
										}, function(isConfirm){
											window.location='index.php?page=pertemuan&kode_mk=$kd_mk'
										});
									 </script>";
						}
				}
				else
				{
					echo "
						<script>
						swal({
									  title: 'File Terlalu Besar',
									  text: 'Max File 5 MB',
									  type: 'warning',
									  showCancelButton: false,
									  cancelButtonText: 'No, Cancel!',
									  confirmButtonText: 'Oke',
									  closeOnConfirm: true
									}, function(isConfirm){
										window.location='index.php?page=pertemuan&kode_mk=$kd_mk'
									});
						</script>";
				}
			}
			else
			{
				// echo "GAGAL EXTENSI FILE";
				echo "
						<script>
						swal({
									  title: 'Extensi File Tidak Di perbolehkan',
									  text: 'Pilih .pdf',
									  type: 'warning',
									  showCancelButton: false,
									  cancelButtonText: 'No, Cancel!',
									  confirmButtonText: 'Oke',
									  closeOnConfirm: true
									}, function(isConfirm){
										window.location='index.php?page=pertemuan&kode_mk=$kd_mk'
									});
						</script>";
			}
    

  }
  else {
    echo "
						<script>
						swal({
									  title: 'Warning',
									  text: 'Kode MK Tidak ditemukan !',
									  type: 'warning',
									  showCancelButton: false,
									  cancelButtonText: 'No, Cancel!',
									  confirmButtonText: 'Oke',
									  closeOnConfirm: true
									}, function(isConfirm){
										window.location='index.php?page=pertemuan&kode_mk=$kd_mk'
									});
						</script>";
  }
}
?>
</body>
</html>
