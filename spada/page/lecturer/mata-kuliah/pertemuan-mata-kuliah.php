<?php
error_reporting(0);
include "../../../../sso-uwg/koneksi/koneksi.php";
?>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <title></title>
  <style type="text/css">
  .fa-input {
    font-family: FontAwesome, 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  .doc {
    width: 100%;
    height: 500px;
  }
  </style>
</head>
<body>
  <section class="content-header">
    <?php
    function rupiah($angka)
    {
      $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
      return $hasil_rupiah;
    }

    function tgl_indo($tanggal)
    {
      $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
      );
      $pecahkan = explode('-', $tanggal);
      return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    $sql = "SELECT * FROM tbl_spada_mk JOIN users_nama ON tbl_spada_mk.nidn = users_nama.nidn WHERE kode_mk = '$_GET[kode_mk]'";
    $data = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
    while ($row = mysqli_fetch_array($data)) {
      $id         = $row['id_pe'];
      $timestamp  = $row['timestamp'];
      $tanggal    = tgl_indo(date("Y-m-d", strtotime($timestamp)));
      $jam        = date("G:i A", strtotime($timestamp));
      $newTanggal = $tanggal . " pukul " . $jam; ?>
      <h1>
        Pertemuan Mata Kuliah
        <small><?php echo $row['kode_mk']; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php?page=mata-kuliah"><i class="fa fa-file-o"></i>Daftar Mata Kuliah</a></li>
        <li class="active">Pertemuan Mata Kuliah</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-sipamas">
            <div class="box-body">
              <dl class="dl-horizontal">
                <dt></dt>
                <dd>
                  <h2 style="margin-bottom:10px"><a href="index.php?page=mata-kuliah" data-toggle="tooltip" data-placement="left" title="Daftar Mata Kuliah"><i class="fa fa-xs fa-arrow-left"></i></a><b> Mata Kuliah - <?php echo $row['kode_mk']; ?></b></h2>
                </dd>
                <dd>
                  <div class="text-left">
                    <a class="btn btn-sm btn-success open_modal_edit" id="<?php echo $row['kode_mk']; ?>"><i class="fa fa-pencil"></i> Edit Mata Kuliah</a>

                    <?php
                    echo "<a onClick='confirm_delete_mk(\"index.php?page=mk-delete&kode_mk=$row[kode_mk]\")' class='btn btn-sm btn-danger'><i class='fa fa-times'></i> Hapus Mata Kuliah</a>"; ?>

                    <a class="btn btn-sm btn-primary open_modal_rps" id="<?php echo $row['kode_mk']; ?>"><i class="fa fa-upload"></i> Upload RPS</a>
                    <?php
                      if(isset($row['file_rps'])){
                    ?>
                      <!-- <a class="btn btn-sm btn-info open_modal_Vrps" id="<?php echo $row['kode_mk']; ?>"><i class="fa fa-eye"></i> View RPS</a> -->
                      <button class="btn btn-sm btn-info" data-target="#ModalViewRps" data-toggle="modal" data-placement="top" title="View RPS"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;View RPS</button>
                    <?php
                      }  
                    ?>

<!-- Modal View RPS-->
<div class="modal fade" id="ModalViewRps" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header header-sipamas">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" align="center"><font color="white"><h4>
          <b>RPS Mata Kuliah - <?php echo $row["nama_mk"]; ?></b>
        </h4></font></h4>
      </div>
      <div class="modal-body">
        <iframe class="doc" src="https://docs.google.com/gview?url=https://sso.widyagama.ac.id/spada/up_rps/<?php echo $row['file_rps']; ?>&embedded=true"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
                  </dd><br>
                  <!-- <dt></dt>
                  <dd><br><h4 style="color:#4d9ae3;"><b>Data Dosen & Mata Kuliah</b></h4></dd> -->
                  <dt></dt>
                  <dd><h4 style="color:#4d9ae3;"><b>Data Dosen</b></h4></dd>
                  <dt>NIDN</dt>
                  <dd><?php echo $row['nidn']; ?></dd>
                  <dt>Nama Dosen</dt>
                  <dd><?php echo $row['nama_dosen']; ?></dd>
                  <dt>Fakultas / Jurusan</dt>
                  <dd><?php echo $row['fakultas'] . " - " . $row['jurusan']; ?></dd><br>
                  <dt></dt>
                  <dd><h4 style="color:#4d9ae3;"><b>Data Mata Kuliah</b></h4></dd>
                  <dt>Kode MK</dt>
                  <dd><?php echo $row['kode_mk']; ?></dd>
                  <dt>Nama Mata Kuliah</dt>
                  <dd><?php echo $row['nama_mk']; ?></dd>
                  <dt>Kode <i>Google Classroom</i></dt>
                  <dd><?php echo $row['kode_classroom']; ?> </dd>
                  <dt>Fakultas / Jurusan</i></dt>
                  <dd><?php echo $row['fak'] . " - " . $row['jur']; ?></dd>
                  <hr>
                  <dt></dt>
                  <dd><h4 style="color:#4d9ae3;"><b>Pertemuan Mata Kuliah</b></h4></dd>
                  <dt></dt>
                  <dd>
                    <a class="btn btn-sm btn-primary open_modal_add" id="<?php echo $row['kode_mk']; ?>"><i class="fa fa-plus-circle"></i> Tambah Pertemuan</a>

                    <?php
                    echo "<a onClick='truncate(\"index.php?page=pertemuan-truncate&kode_mk=$row[kode_mk]\")' class='btn btn-sm btn-danger'><i class='fa fa-times'></i> Kosongkan Tabel Pertemuan</a>";
                    ?>
                  </dd><br>
                  <table class="table table-condensed table-striped table-striped table-hover" id="tabel-data">
                    <thead >
                      <tr>
                        <th class="text-center" style="width: 110px">Tanggal</th>
                        <th class="text-center">Materi</th>
                        <th class="text-center" style="width: 190px">Tugas</th>
                        <th class="text-center" style="width: 25px">Mahasiswa </th>
                        <th class="text-center" style="width: 70px">Aksi</th>
                      </tr>
                    </thead>
                    <?php

                    $sql = "SELECT * FROM tbl_spada_pertemuan WHERE kode_mk = '$row[kode_mk]' && nidn = '$_SESSION[username]' ORDER BY tanggal DESC";
                    $data1 = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
                    while ($row1 = mysqli_fetch_array($data1)) {
                      ?>
                      <tr>
                        <td class="text-center"><?php echo tgl_indo($row1['tanggal']); ?></td>
                        <td><?php echo $row1['materi']; ?></td>
                        <td><?php echo $row1['tugas']; ?></td>
                        <td class="text-center"><?php echo $row1['mhs_turn_in'] ?></td>

                        <td class="text-center">
                          <span data-toggle="modal" data-target="#myModal<?php echo $row1['id']; ?>">
                            <a href="#" type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Edit Pertemuan" ><i class="fa fa-pencil"></i></a>
                          </span>
                          <?php
                          echo "<a onClick='confirm_delete_pt(\"index.php?page=pertemuan-delete&kode_mk=$row1[kode_mk]&id=$row1[id]\")' class='btn btn-xs btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus Pertemuan'><i class='fa fa-trash'></i> </a>"; ?>
                        </td>
                      </tr>

                      <!-- modal tombol pertemuan edit -->
                      <div class="modal fade" id="myModal<?php echo $row1['id']; ?>" role="dialog">
                        <div class="modal-dialog">
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header header-sipamas">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title text-center" ><font color="white"><h4>
                                <b>Edit Pertemuan ke  - <?php echo substr($row1['id'], -2);?><br>Mata Kuliah <?php echo $row1['kode_mk']?></b>
                              </h4></font></h4>
                            </div>
                            <div class="modal-body">
                              <p class="text-center"><font color="red"><b>Perhatian!</b> Pastikan kembali data yang diisi adalah benar dan lengkap.</font></p><br>
                              <form id="update_form" role="form" action="index.php?page=pertemuan-update" method="post" class="registration-form">
                                <?php
                                $idKode = $row1['id'];
                                $query_edit = mysqli_query($koneksi, "SELECT * FROM tbl_spada_pertemuan WHERE id='$idKode'");
                                //$result = mysqli_query($conn, $query);
                                while ($data_edit = mysqli_fetch_array($query_edit)) {
                                  // print_r($data_edit);
                                  ?>
                                  <input style="width:536px" name="id" type="hidden" class="form-control text-center" value="<?php echo $data_edit["id"]; ?>" readonly/>
                                  <input style="width:536px" name="kode_mk" type="hidden" class="form-control text-center" value="<?php echo $data_edit["kode_mk"]; ?>" readonly/>

                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label>Materi*</label>
                                      <div class="input-group">
                                        <textarea style="width:536px" name="materi" class="form-control" required><?php echo $data_edit["materi"]; ?></textarea>
                                        <small class="form-text text-muted"><i>Tulis deskripsi singkat materi yang diberikan pada kelas.</i></small class="form-text text-muted">
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label>Tugas</label>
                                        <div class="input-group">
                                          <textarea style="width:536px" name="tugas" class="form-control"><?php echo $data_edit["tugas"]; ?></textarea>
                                          <small class="form-text text-muted"><i>Tulis deskripsi singkat tugas yang diberikan kepada mahasiswa.<br>Jika tidak ada tugas, kosongi kolom ini.</i></small class="form-text text-muted">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label>Tanggal*</label>
                                          <div class="input-group">
                                            <input style="width:250px" name="tanggal" type="date" class="form-control" value="<?php echo $data_edit['tanggal'];?>" required/>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label>Mahasiswa Turn In</label>
                                          <div class="input-group">
                                            <input style="width:250px" name="mhs_turn_in" type="text" class="form-control" value="<?php echo $data_edit['mhs_turn_in'];?>" onkeypress="return hanyaAngka(event)"/>
                                            <small class="form-text text-muted"><i>Jumlah mahasisa yang mengumpulkan tugas.</i></small class="form-text text-muted">
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <hr>
                                        </div>
                                        <?php
                                      }
                                      //mysql_close($host);
                                      ?>
                                      <!-- penutup while $data_edit -->
                                    </div>
                                    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                                      <button type="reset" class="btn btn-sm btn-primary" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-left"></i> Kembali</button>
                                      <!-- <button type="submit" id="submit" form="update_form" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button> -->
                                      <input type="submit" name="update" value="&#xf0c7; Simpan" class="btn btn-sm btn-success fa-input" />
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                            <!-- end modal edit -->
                            <?php
                          }
                          ?>
                        </table>
                        <hr>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
              }
              ?>
            </section>
          </body>
          </html>

          <!-- Modal Popup Add Pertemuan-->
          <div id="ModalAdd" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>

          <!-- Modal Popup Edit -->
          <div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>

          <!-- Modal Popup Delete Mata Kuliah-->
          <div id="ModalDeleteMK" class="modal modal-warning fade">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" style="text-align:center;">Apakah anda yakin menghapus data ini ?</h4>
                </div>
                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                  <button type="button" class="btn btn-sm btn-outline" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Kembali</button>
                  <a href="#" class="btn btn-sm btn-outline" id="delete_mk_link"><i class="fa fa-trash"></i> Hapus</a>
                </div>
              </div>
            </div>
          </div>

          <!-- Modal Popup Delete Pertemuan-->
          <div id="ModalDeletePT" class="modal modal-warning fade">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" style="text-align:center;">Apakah anda yakin menghapus data ini ?</h4>
                </div>
                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                  <button type="button" class="btn btn-sm btn-outline" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Kembali</button>
                  <a href="#" class="btn btn-sm btn-outline" id="delete_pertemuan_link"><i class="fa fa-trash"></i> Hapus</a>
                </div>
              </div>
            </div>
          </div>

          <!-- Modal Popup Truncate Pertemuan-->
          <div id="ModalTruncate" class="modal modal-danger fade" >
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" style="text-align:center;">Apakah anda yakin mengosongkan tabel ini ?</h4>
                </div>
                <div class="modal-body">
                  <p align="center">Anda akan menghapus SELURUH pertemuan pada Mata Kuliah <?php echo $_GET['kode_mk']; ?><br>Peringatan! Data yang telah dihapus tidak dapat dikembalikan.</p>
                </div>
                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                  <button type="button" class="btn btn-sm btn-outline" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Kembali</button>
                  <a href="#" class="btn btn-sm btn-outline" id="truncate_link"><i class="fa fa-times"></i> Kosongkan Tabel</a>
                </div>
              </div>
            </div>
          </div>

          <script>
          $(document).ready(function(){
            var table = $('#tabel-data').DataTable({
              'info'        : true,
              'paging'      : false,
              'ordering'    : false,
              'searching'   : false,
              'pageLength'  : 10,
              fixedHeader: {
                header: true,
                footer: true
              },
              'language' : {'emptyTable'  : 'Tidak ada data pertemuan pada mata kuliah ini',
              'info'     : 'Menampilkan _START_ hingga _END_ dari _TOTAL_ data'
            },
          });
        });
        </script>

        <!-- Javascript Edit Mata Kuliah-->
        <script>
        $(document).ready(function () {

          $(".open_modal_edit").click(function(edit) {
            var o = $(this).attr("id");
            $.ajax({
              url: "mata-kuliah/modal-mata-kuliah/modal-mk-edit.php",
              type: "GET",
              data : {kode_mk: o,},
              success: function (ajaxData){
                $("#ModalEdit").html(ajaxData);
                $("#ModalEdit").modal('show',{backdrop: 'true'});
              }
            });
          });
        });
        </script>

        <!-- Javascript Add Pertemuan-->
        <script>
        $(document).ready(function () {

          $(".open_modal_add").click(function(edit) {
            var o = $(this).attr("id");
            $.ajax({
              url: "mata-kuliah/modal-pertemuan/modal-pertemuan-add.php",
              type: "GET",
              data : {kode_mk: o,},
              success: function (ajaxData){
                $("#ModalAdd").html(ajaxData);
                $("#ModalAdd").modal('show',{backdrop: 'true'});
              }
            });
          });
        });
        </script>

        <!-- Javascript Add RPS-->
        <script>
        $(document).ready(function () {

          $(".open_modal_rps").click(function(edit) {
            var o = $(this).attr("id");
            $.ajax({
              url: "mata-kuliah/modal-rps/modal-rps-add.php",
              type: "GET",
              data : {kode_mk: o,},
              success: function (ajaxData){
                $("#ModalAdd").html(ajaxData);
                $("#ModalAdd").modal('show',{backdrop: 'true'});
              }
            });
          });
        });
        </script>

        <!-- Javascript Delete Mata Kuliah -->
        <script>
        function confirm_delete_mk(delete_url){
          $("#ModalDeleteMK").modal('show', {backdrop: 'static'});
          document.getElementById('delete_mk_link').setAttribute('href', delete_url);
        }
        </script>

        <!-- Javascript Delete pertemuan -->
        <script>
        function confirm_delete_pt(delete_url){
          $("#ModalDeletePT").modal('show', {backdrop: 'static'});
          document.getElementById('delete_pertemuan_link').setAttribute('href', delete_url);
        }
        </script>

        <!-- Javascript Truncate Pertemuan -->
        <script>
        function truncate(delete_url){
          $("#ModalTruncate").modal('show', {backdrop: 'static'});
          document.getElementById('truncate_link').setAttribute('href', delete_url);
        }
        </script>

        <script>
        		function hanyaAngka(evt) {
        		  var charCode = (evt.which) ? evt.which : event.keyCode
        		   if (charCode > 31 && (charCode < 48 || charCode > 57))

        		    return false;
        		  return true;
        		}
        </script>
