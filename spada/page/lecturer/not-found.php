<body>
  <section class="content-header">
    <h1>
      404 Error Page
      <!-- <small>Dosen</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-close"></i>404 Error Page</a></li>
    </ol>
    </section>
  <section class="content">
  <div class="error-page">
    <br><br>
    <h2 class="headline text-yellow"> 404</h2>

    <div class="error-content">
      <h3><i class="fa fa-warning text-yellow"></i> Oops! Halaman tidak ditemukan. ☹️</h3>

      <p>
        Kami tidak dapat menemukan halaman yang anda cari. Sementara itu, anda dapat kembali ke halaman utama.
      </p>

      <form class="search-form">
        <div class="input-group">
          <input type="text" name="search" class="form-control" placeholder="Search">

          <div class="input-group-btn">
            <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
            </button>
          </div>
        </div>
        <!-- /.input-group -->
      </form>
    </div>
    <!-- /.error-content -->
  </div>
  <!-- /.error-page -->
</section>
</body>
