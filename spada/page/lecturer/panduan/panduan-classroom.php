<?php
include "../../../../sso-uwg/koneksi/koneksi.php";
?>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <title></title>
</head>
<style>
body {
    height:100%;
    width:100%;
    margin:0;

}
.h_iframe iframe {
    width:100%;
    height:100%;

}
.h_iframe {
    height: 100%;
    width:100%;

}
</style>

<body>
  <section class="content-header">

    <h1>
      Panduan Penggunaan
      <small>Google Classroom</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-book"></i>Panduan</a></li>
      <li class="active">Google Classroom</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-sipamas">
          <div class="box-body">
            <!-- <div class="h_iframe"> -->
              <iframe src="../../file/Panduan Google Classroom.pdf" scrolling="yes" width="100%" height="800px" onload="resizeIframe(this)"></iframe>
            <!-- </div> -->
          </div>
        </div>
      </div>
    </div>
  </section>
</body>
</html>

<script>
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
  }
</script>
