<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../../../assets/plugins/Sweetalert/dist/sweetalert.css">
	<script type="text/javascript" src="../../../../assets/plugins/Sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>

	<?php
	include "../../../koneksi/koneksi.php";
	include "../../spada/page/lecturer/profile/profile.php";

	$nidn   	     = $_POST['nidn'];
	$nama_dosen	   = mysqli_real_escape_string($koneksi, $_POST['nama_dosen']);
  $fakultas	     = mysqli_real_escape_string($koneksi, $_POST['fakultas']);
  $jurusan	     = mysqli_real_escape_string($koneksi, $_POST['jurusan']);
  $telp	         = mysqli_real_escape_string($koneksi, $_POST['telp']);
  $email	       = mysqli_real_escape_string($koneksi, $_POST['email']);


	$modalquery = mysqli_query($koneksi, "UPDATE users JOIN users_nama ON users.username=users_nama.nidn SET users_nama.nama_dosen='$nama_dosen',users_nama.fakultas='$fakultas',users_nama.jurusan='$jurusan',users.email='$email',users.telp='$telp' WHERE users.username='$nidn'");

	if ($modalquery == TRUE)
	{
		echo "
		<script>
		swal({
			title: 'Sukses',
			text: 'Data berhasil disimpan',
			type: 'success',
			showCancelButton: false,
			cancelButtonText: 'No, Cancel!',
			confirmButtonText: 'Oke',
			closeOnConfirm: true
		}, function(isConfirm){
			window.location='index.php?page=profile'
		});
		</script>";

	}
	else
	{
		echo "
		<script>
		swal({
			title: 'Error',
			text: 'Data gagal disimpan',
			type: 'error',
			showCancelButton: false,
			cancelButtonText: 'No, Cancel!',
			confirmButtonText: 'Oke',
			closeOnConfirm: true
		}, function(isConfirm){
			window.location='index.php?page=profile'
		});
		</script>";
	}
	?>

</body>
</html>
