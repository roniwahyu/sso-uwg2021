<?php
include "../../../../../../sso-uwg/koneksi/koneksi.php";
$nidn	= $_GET["nidn"];
?>

  <div class="modal-dialog modal-sm" role="document">
  <div class="modal-content">
    <div class="modal-header header-sipamas">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title text-center" style="color:#fff;"><b>Ganti Password</b></h4>
    </div>
    <div class="modal-body">
      <form id="password_update" method="post" action="index.php?page=password-update" enctype="multipart/form-data">
        <div class="form-group">
          <label>Tulis Password Baru</label>
          <input type="hidden" name="nidn" value="<?php echo $nidn ?>" readonly/>
          <div class="input-group">
            <input style="width:233px" type="password" name="password" id="no_surat" class="form-control pwd" value=""/>
            <span class="input-group-btn">
            <button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
          </span>
          </div>
          </div>
        </form>
      </div>

      <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
        <button type="reset" class="btn btn-sm btn-primary" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Kembali</button>
        <button form="password_update" type="submit" class="btn btn-sm btn-success"> <i class="fa fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>

<script>
$(".reveal").on('click',function() {
    var $pwd = $(".pwd");
    if ($pwd.attr('type') === 'password') {
        $pwd.attr('type', 'text');
    } else {
        $pwd.attr('type', 'password');
    }
});
</script>
