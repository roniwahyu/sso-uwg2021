<?php
include "../../../../../../sso-uwg/koneksi/koneksi.php";
$nidn	= $_GET["nidn"];

$query = mysqli_query($koneksi, "SELECT users_nama.nidn,users_nama.nama_dosen,users_nama.fakultas,users_nama.jurusan,users.email,users.telp FROM users,users_nama WHERE users.username='$nidn' AND nidn='$nidn'");
if($query == false){
  die ("Terjadi Kesalahan : ". mysqli_error($koneksi));
}
while($r = mysqli_fetch_array($query)){
  ?>

  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header header-sipamas">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" align="center"><font color="white"><h4>
          <b>Edit Profile NIDN - <?php echo $r["nidn"]; ?></b>
        </h4></font></h4>
      </div>

      <div class="modal-body">
        <p class="text-center"><font color="red"><b>Perhatian!</b> Pastikan kembali data yang diisi adalah benar dan lengkap.</font></p><br>
        <form id="profile_update" action="index.php?page=profile-update" name="modal_popup" enctype="multipart/form-data" method="post">

          <input style="width:536px" name="nidn" type="hidden" class="form-control text-center" value="<?php echo $r["nidn"]; ?>" readonly/>

          <div class="col-md-12">
            <div class="form-group">
              <label>Nama Lengkap & Gelar*</label>
              <div class="input-group col-md-12">
                <input name="nama_dosen" type="text" class="form-control" value="<?php echo $r["nama_dosen"]; ?>" required/>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Fakultas*</label>
              <div class="input-group">
                <?php $fakultas = $r['fakultas']; ?>
                <select class="form-control" style="width: 250px;" name="fakultas" id="fakultas" required>
                  <option <?php echo ($fakultas == 'Fakultas Ekonomi') ? "selected": "" ?>>Fakultas Ekonomi</option>
                  <option <?php echo ($fakultas == 'Fakultas Hukum') ? "selected": "" ?>>Fakultas Hukum</option>
                  <option <?php echo ($fakultas == 'Fakultas Pertanian') ? "selected": "" ?>>Fakultas Pertanian</option>
                  <option <?php echo ($fakultas == 'Fakultas Teknik') ? "selected": "" ?>>Fakultas Teknik</option>
                  <option <?php echo ($fakultas == 'Pascasarjana') ? "selected": "" ?>>Pascasarjana</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label>No. Telp/HP(WA)*</label>
              <div class="input-group">
                <input style="width:250px" name="telp" type="text" class="form-control" value="<?php echo $r["telp"]; ?>" required/>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Jurusan*</label>
              <div class="input-group">
                <?php $jurusan = $r['jurusan'];?>
                <select class="form-control" style="width: 250px;" name="jurusan" id="jurusan" required>
                  <option <?php echo ($jurusan == 'D3 Keuangan & Perbankan') ? "selected": "" ?>>D3 Keuangan & Perbankan</option>
                  <option <?php echo ($jurusan == 'D3 Otomotif') ? "selected": "" ?>>D3 Otomotif</option>
                  <option disabled>----</option>
                  <option <?php echo ($jurusan == 'S1 Akuntansi') ? "selected": "" ?>>S1 Akuntansi</option>
                  <option <?php echo ($jurusan == 'S1 Manajemen') ? "selected": "" ?>>S1 Manajemen</option>
                  <option <?php echo ($jurusan == 'S1 Ilmu Hukum') ? "selected": "" ?>>S1 Ilmu Hukum</option>
                  <option <?php echo ($jurusan == 'S1 Agrobisnis') ? "selected": "" ?>>S1 Agrobisnis</option>
                  <option <?php echo ($jurusan == 'S1 Agroteknologi') ? "selected": "" ?>>S1 Agroteknologi</option>
                  <option <?php echo ($jurusan == 'S1 Teknologi Hasil Pertanian') ? "selected": "" ?>>S1 Teknologi Hasil Pertanian</option>
                  <option <?php echo ($jurusan == 'S1 Teknik Elektro') ? "selected": "" ?>>S1 Teknik Elektro</option>
                  <option <?php echo ($jurusan == 'S1 Teknik Informatika') ? "selected": "" ?>>S1 Teknik Informatika</option>
                  <option <?php echo ($jurusan == 'S1 Teknik Mesin') ? "selected": "" ?>>S1 Teknik Mesin</option>
                  <option <?php echo ($jurusan == 'S1 Teknik Industri') ? "selected": "" ?>>S1 Teknik Industri</option>
                  <option <?php echo ($jurusan == 'S1 Teknik Sipil') ? "selected": "" ?>>S1 Teknik Sipil</option>
                  <option disabled>----</option>
                  <option <?php echo ($jurusan == 'S2 Manajemen') ? "selected": "" ?>>S2 Manajemen</option>
                  <option <?php echo ($jurusan == 'S2 Hukum') ? "selected": "" ?>>S2 Hukum</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label>E-Mail*</label>
              <div class="input-group">
                <input style="width:250px" name="email" type="email" class="form-control" value="<?php echo $r["email"]; ?>" required/>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <hr>
          </div>
        </form>
        <br>
      </div>
      <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
        <button type="reset" class="btn btn-sm btn-primary" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-left"></i> Kembali</button>
        <button form=profile_update class="btn btn-sm btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
  <?php
}
?>
