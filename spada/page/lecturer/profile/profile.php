<?php

include "../../../../sso-uwg/koneksi/koneksi.php";

function getWeeks($date, $rollover)
{
  $cut = substr($date, 0, 8);
  $daylen = 86400;

  $timestamp = strtotime($date);
  $first = strtotime($cut . "00");
  $elapsed = ($timestamp - $first) / $daylen;

  $weeks = 1;

  for ($i = 1; $i <= $elapsed; $i++) {
    $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
    $daytimestamp = strtotime($dayfind);

    $day = strtolower(date("l", $daytimestamp));

    if ($day == strtolower($rollover))  $weeks++;
  }

  return $weeks;
}


// function getWeeks2($tgl_awal, $tgl_akhir, $rollover){
//     $detik = 24 * 3600;
//     $tgl_awal = strtotime($tgl_awal);
//     $tgl_akhir = strtotime($tgl_akhir);

//     $minggu = 0;

//      for ($i = $tgl_awal; $i <= $tgl_akhir; $i++)
//         {
//             $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
//             $daytimestamp = strtotime($dayfind);

//             $day = strtolower(date("l", $daytimestamp));

//             if($day == strtolower($rollover))  $weeks ++;
//         }

//     return $weeks;

//     // for ($i=$tgl_awal; $i < $tgl_akhir; $i += $detik)
//     // {
//     //     if (date('w', $i) == '0'){
//     //         $minggu++;

//     //     }
//     // }
//     // return $minggu;
// }

function tgl_indo($tanggal)

{

  $bulan = array(

    1 =>   'Januari',

    'Februari',

    'Maret',

    'April',

    'Mei',

    'Juni',

    'Juli',

    'Agustus',

    'September',

    'Oktober',

    'November',

    'Desember'

  );

  $pecahkan = explode('-', $tanggal);

  return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}

?>



<html>

<head>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <title></title>

</head>

<body>

  <section class="content-header">

    <?php

    $nidn = $_SESSION['username'];

    $sql = "SELECT users_nama.nidn,users_nama.nama_dosen,users_nama.fakultas,users_nama.jurusan,users.email,users.telp FROM users,users_nama WHERE users.username='$nidn' AND nidn='$nidn'";

    $data = mysqli_query($koneksi, $sql);

    while ($row = mysqli_fetch_array($data)) {

    ?>

      <h1>

        Profile Dosen

        <!-- <small>Dosen</small> -->

      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-user"></i>Profile Dosen</a></li>

      </ol>

  </section>

  <!-- Main content -->

  <section class="content">



    <div class="row">

      <div class="col-xs-12">

        <div class="alert alert-warning alert-dismissible">

          <button type="button" class="close" data-dismiss="alert" data-toggle="tooltip" data-placement="left" title="" aria-hidden="true" data-original-title="Tutup">×</button>

          <h4><i class="icon fa fa-bullhorn"></i> Pengumuman!</h4>
          <?php
          // date_default_timezone_set('asia/jakarta');
          // // $date = date('Y-m-d');
          // $date = date('W');
          // $minggu=1;
          // if($date==40){
          //   $minggu=40-39;
          //
          // }else{
          //   $minggu=$date-39;
          // }
          // print_r($date);
          // $ddate = NOW();
          // $date = new DateTime($ddate);
          // $week = $date->format("W");
          // echo "Weeknummer: $week";
          ?>
          <p>

            [1] Semester aktif 2020/2021 Genap.<br>

            <!--[2] Saat ini adalah perkuliahan minggu ke-<?php $tgl = date('Y-m-d');
                                                          echo getWeeks($tgl, 'monday');
                                                          echo "<meta http-equiv='refresh' content='5000'>"; ?>.<br>-->


            <!-- <b>[2] Pekan UTS dilaksanakan pada tanggal 20 - 25 April 2020.</b><br>

                <b>[3] UTS dilaksanakan secara daring (<i>online</i>)</b>. -->

          </p>

        </div>

        <div class="callout callout-success">

          <h4><b>Selamat Datang! <?php echo $row['nama_dosen']; ?></b></h4>



          <p>Anda telah berhasil login ke sistem <b>Monev E-Learning Universitas Widyagama Malang</b>. Periksa kembali data pribadi anda. Pastikan anda telah membaca <a href="index.php?page=panduan">panduan pengisian</a> sebelum mulai mengisi.</p>

        </div>

        <div class="box box-sipamas">

          <!-- /.box-header -->

          <div class="box-body">

            <dl class="dl-horizontal">

              <dt></dt>

              <dd>
                <h4 style="color:#4d9ae3;"><b>Data Pribadi</b></h4>
              </dd>

              <dt>NIDN</dt>

              <dd><?php echo $row['nidn']; ?></dd>

              <dt>Nama Lengkap & Gelar</dt>

              <dd><?php echo $row['nama_dosen']; ?></dd>

              <dt>Fakultas / Program Studi </dt>

              <dd><?php echo $row['fakultas'] . " - " . $row['jurusan']; ?></dd>

              <dt>Kontak</dt>

              <dd><i class="fa fa-envelope-o"></i> <?php echo $row['email']; ?> &nbsp <i class="fa fa-phone"></i> <?php echo $row['telp']; ?></dd>

              <hr>

              <dd>

                <a class="btn btn-sm btn-success open_modal_profile" id="<?php echo $row['nidn']; ?>"><i class="fa fa-pencil"></i> Edit Profile</a>
                <a class="btn btn-sm btn-success open_modal_password" id="<?php echo $row['nidn']; ?>"><i class="fa fa-key"></i> Ganti Password</a>

              </dd>

              <hr>

            </dl>

          </div>

        </div>

      </div>

    </div>

  <?php

    } // end while $row

  ?>

  </section>

</body>

</html>



<!-- Modal Popup Update Profile -->

<div id="ModalEditProfile" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>



<!-- Modal Popup Update Password -->

<div id="ModalEditPassword" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>



<!-- Javascript Update Profile-->

<script>
  $(document).ready(function() {

    $(".open_modal_profile").click(function(edit) {

      var o = $(this).attr("id");

      $.ajax({

        url: "profile/modal-profile/modal-profile-update.php",

        type: "GET",

        data: {
          nidn: o,
        },

        success: function(ajaxData) {

          $("#ModalEditProfile").html(ajaxData);

          $("#ModalEditProfile").modal('show', {
            backdrop: 'true'
          });

        }

      });

    });

  });
</script>



<!-- Javascript Update Password-->

<script>
  $(document).ready(function() {

    $(".open_modal_password").click(function(edit) {

      var o = $(this).attr("id");

      $.ajax({

        url: "profile/modal-profile/modal-password-update.php",

        type: "GET",

        data: {
          nidn: o,
        },

        success: function(ajaxData) {

          $("#ModalEditPassword").html(ajaxData);

          $("#ModalEditPassword").modal('show', {
            backdrop: 'true'
          });

        }

      });

    });

  });
</script>