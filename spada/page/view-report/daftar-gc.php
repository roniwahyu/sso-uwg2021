<div class="container">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Daftar Kode <i>Google Classroom</i>
      <small><?php echo $fak . ' ' .$jur?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-file-o"></i> Daftar Kode <i>Google Classroom</i></a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="alert alert-warning alert-dismissible">
          <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> -->
          <h4><i class="icon fa fa-bullhorn"></i> Pengumuman !</h4>
          <p>
            <b>[1] Saat ini adalah perkuliahan minggu ke 8.</b><br>
            <b>[2] Pekan UTS dilaksanakan pada tanggal 20 - 25 April 2020.</b><br>
            <b>[3] UTS dilaksanakan secara daring (<i>online</i>)</b>.
          </p>
        </div>
        <div class="box box-sipamas">
          <div class="box-body">
            <div class="col-md-12">
              <p>Pilih fakultas & jurusan anda untuk melihat kode <i>Google Classroom</i> dari mata kuliah yang anda ikuti. Segera bergabung ke dalam <i>classroom</i> tersebut, jika kode tidak bisa segera hubungi dosen anda. Untuk melihat kode <i>Google Classroom</i> mata kuliah umum pilih fakultas anda.</p><br>
            </div>
            <form method="POST" class="form-user" id="form-user">
              <div class="col-md-6">
                <label>Fakultas</label>
                <select name="fakultas" id="fakultas" class="form-control form-control-sm" required>
                  <option value=""> --- Pilih Fakultas --- </option>
                  <option value="Fakultas Ekonomi">Fakultas Ekonomi</option>
                  <option value="Fakultas Teknik">Fakultas Teknik</option>
                  <option value="Fakultas Hukum">Fakultas Hukum</option>
                  <option value="Fakultas Pertanian">Fakultas Pertanian</option>
                  <option value="Pascasarjana">Pascasarjana</option>
                </select>
              </div>
              <div class="col-md-6">
                <label>Jurusan</label>
                <select name="jurusan" id="jurusan" class="form-control form-control-sm">
                  <option value=""> --- Pilih Jurusan --- </option>
                  <option value="D3 Keuangan & Perbankan">D3 Keuangan & Perbankan</option>
                  <option value="D3 Otomotif">D3 Otomotif</option>
                  <option disabled>----</option>
                  <option value="S1 Akuntansi">S1 Akuntansi</option>
                  <option value="S1 Manajemen">S1 Manajemen</option>
                  <option value="S1 Ilmu Hukum">S1 Ilmu Hukum</option>
                  <option value="S1 Agrobisnis">S1 Agrobisnis</option>
                  <option value="S1 Agroteknologi">S1 Agroteknologi</option>
                  <option value="S1 Teknologi Hasil Pertanian">S1 Teknologi Hasil Pertanian</option>
                  <option value="S1 Teknik Elektro">S1 Teknik Elektro</option>
                  <option value="S1 Teknik Informatika">S1 Teknik Informatika</option>
                  <option value="S1 Teknik Mesin">S1 Teknik Mesin</option>
                  <option value="S1 Teknik Industri">S1 Teknik Industri</option>
                  <option value="S1 Teknik Sipil">S1 Teknik Sipil</option>
                  <option disabled>----</option>
                  <option value="S2 Manajemen">S2 Manajemen</option>
                  <option value="S2 Hukum">S2 Hukum</option>
                </select>
              </div>
              <div class="col-md-12">
                <br>
                <button type="submit" class="btn btn-sm btn-primary" id="tombol-rekap" name="rekap"><span class="glyphicon glyphicon-eye-open"></span> Tampilkan Data </button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <?php
      include "../../../../sso-uwg/koneksi/koneksi.php";

      if(isset($_POST['rekap'])){
        $fakultas = $_POST['fakultas'];
        $jurusan = $_POST['jurusan'];

        $no = 1;
        $sql = "SELECT * FROM tbl_spada_mk JOIN users_nama ON tbl_spada_mk.nidn = users_nama.nidn WHERE  (tbl_spada_mk.fak LIKE '%$fakultas%' AND tbl_spada_mk.jur LIKE '%$jurusan%') ORDER BY kode_mk ASC";

        $data = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
      ?>
      <div class="col-xs-12">
        <div class="box box-sipamas">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><b>Daftar Mata Kuliah <?php echo $fakultas . ' ' . $jurusan;?> </b></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" data-placement="top" title="Tutup"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" id="tabel-data">
              <thead>
                <tr>
                  <th class="text-center" style="width: 10px">No</th>
                  <th class="text-center" style="width: 90px">Kode MK</th>
                  <th class="text-center">Mata Kuliah</th>
                  <th class="text-center" style="width: 20px">Kode <i>Classroom</i></th>
                  <th class="text-center" style="width: 350px">NIDN / Nama Dosen</th>
                </tr>
              </thead>
              <?php
              while ($row = mysqli_fetch_array($data)) {
              ?>
                <tr>
                  <td class="text-center"><?php echo $no++; ?></td>
                  <td><?php echo $row['kode_mk'];; ?></td>
                  <td>
                    <b><?php echo $row['nama_mk']; ?></b>
                  </td>
                  <td class="text-center"><?php echo $row['kode_classroom']; ?></td>
                  <td><?php echo $row['nidn'] . " / " . $row['nama_dosen']; ?></td>
                </tr>
                <?php
              } //akhir while
              ?>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>
    <?php
  } //akhir while
  ?>
    </div>
  </section>
</div>

<script>
$(document).ready(function(){
  var table = $('#tabel-data').DataTable({
    'info'  : true,
    'ordering' : false,
    'pageLength' : 10,
    fixedHeader: {
      header: true,
      footer: true
    },
    'language' : {'emptyTable'  : 'Tidak ada data yang tersedia pada tabel ini',
    'info'        : 'Menampilkan _START_ hingga _END_ dari _TOTAL_ data',
    'infoEmpty'   : 'Menampilkan 0 hingga 0 dari 0 data',
    'infoFiltered': '(dicari dari total _MAX_ data)',
    'search'      : 'Pencarian data:',
    'lengthMenu'  : 'Tampilkan _MENU_ data',
    'zeroRecords' : 'Tidak ditemukan data yang cocok',
    'paginate'    : {
      'first'   : 'Pertama',
      'last'    : 'Terakhir',
      'next'    : 'Selanjutnya',
      'previous': 'Sebelumnya'
    },
  },
});
});
</script>
