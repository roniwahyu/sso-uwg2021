<?php
include "../../../../sso-uwg/koneksi/koneksi.php";
include "../../../../sso-uwg/enkripsi.php";

$fak = base64_decode($_GET['fak']);
$jur = base64_decode($_GET['jur']);
?>

<div class="container">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Daftar Mata Kuliah
      <?php echo $fak . ' ' . $jur; ?>
      <small><?php echo $fak . ' ' .$jur?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-files-o"></i> Daftar Mata Kuliah</a></li>
      <li class="active"><?php echo $fak . ' ' .$jur?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-sipamas">
      <div class="box-body">
        <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-condensed table-bordered table-hover" id="tabel-data">
            <thead>
              <tr>
                <th class="text-center" style="width: 10px">No</th>
                <th class="text-center" style="width: 70px">Kode MK</th>
                <th class="text-center">Mata Kuliah</th>
                <th class="text-center" style="width: 20px">Kode <i>Classroom</i></th>
                <th class="text-center" style="width: 10px">Jumlah Pertemuan</th>
                <th class="text-center" style="width: 280px">NIDN / Nama Dosen</th>
                <th class="text-center" style="width: 20px">Aksi</th>
              </tr>
            </thead>
            <?php
            $no = 1;
            $sql = "SELECT * FROM tbl_spada_mk JOIN users_nama ON tbl_spada_mk.nidn = users_nama.nidn WHERE tbl_spada_mk.fak LIKE '%$fak%' AND tbl_spada_mk.jur LIKE '%$jur%'";
            $data = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
            while ($row = mysqli_fetch_array($data)) {
            ?>
              <tr>
                <td class="text-center"><?php echo $no++; ?></td>
                <td class="text-center"><?php echo $row['kode_mk'];; ?></td>
                <td>
                  <b><?php echo $row['nama_mk']; ?></b>
                </td>
                <td class="text-center"><?php echo $row['kode_classroom']; ?></td>
                <td class="text-center">
                  <?php
                  $sql2 = "SELECT COUNT(kode_mk) AS num FROM tbl_spada_pertemuan WHERE (kode_mk = '$row[kode_mk]' AND nidn = '$row[nidn]')";
                  $data2 = mysqli_query($koneksi, $sql2) or die(mysqli_error($koneksi));
                  $row2 = mysqli_fetch_array($data2);
                  echo $row2['num'];
                  ?>

                </td>
                <td class="text-center"><?php echo $row['nidn'] . " / " . $row['nama_dosen']; ?></td>
                <td class="text-center">
                  <a class="btn btn-xs btn-primary open_modal_edit" id="<?php echo $row['kode_mk']; ?>" data-toggle="tooltip" data-placement="top" title="Lihat Pertemuan"><i class="fa fa-info-circle"></i></a>
                </td>
              </tr>
              <?php
            } //akhir while
            ?>
          </table>
        </div>
      </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>

<!-- Modal Popup Edit -->
<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>

<!-- Javascript Edit Mata Kuliah-->
<script>
$(document).ready(function () {

  $(".open_modal_edit").click(function(edit) {
    var o = $(this).attr("id");
    $.ajax({
      url: "modal-view-pertemuan.php",
      type: "GET",
      data : {kode_mk: o,},
      success: function (ajaxData){
        $("#ModalEdit").html(ajaxData);
        $("#ModalEdit").modal('show',{backdrop: 'true'});
      }
    });
  });
});
</script>


<script>
$(document).ready(function(){
  var table = $('#tabel-data').DataTable({
    'info'  : true,
    'ordering' : false,
    'pageLength' : 10,
    fixedHeader: {
      header: true,
      footer: true
    },
    'language' : {'emptyTable'  : 'Tidak ada data yang tersedia pada tabel ini',
    'info'        : 'Menampilkan _START_ hingga _END_ dari _TOTAL_ data',
    'infoEmpty'   : 'Menampilkan 0 hingga 0 dari 0 data',
    'infoFiltered': '(dicari dari total _MAX_ data)',
    'search'      : 'Pencarian data:',
    'lengthMenu'  : 'Tampilkan _MENU_ data',
    'zeroRecords' : 'Tidak ditemukan data yang cocok',
    'paginate'    : {
      'first'   : 'Pertama',
      'last'    : 'Terakhir',
      'next'    : 'Selanjutnya',
      'previous': 'Sebelumnya'
    },
  },
});
});
</script>
