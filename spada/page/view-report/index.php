<?php
error_reporting(0);
include('../sso-uwg/koneksi/koneksi.php');
include('../sso-uwg/enkripsi.php');
include('login_session.php');
date_default_timezone_set("Asia/Jakarta");
?>

<!DOCTYPE html>
<html>
<?php include "../../struktur/head.php"; ?>
<body class="skin-sipamas layout-top-nav" style="height: auto; min-height: 100%;">
  <div class="wrapper" style="height: auto; min-height: 100%;">
    <?php include "../../struktur/header_view_report.php" ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <?php
      if(isset($_GET['view'])){
        $view = $_GET['view'];
        switch ($view) {
          // view report rekap pertemuan mata kuliah
          case 'pertemuan':
            include 'rekap-pertemuan-mk.php';
            break;
          // view report daftar mata kuliah
          case 'daftar-mk':
            include 'daftar-mk.php';
            break;
          // view report daftar google classroom
          case 'classroom':
            include 'daftar-gc.php';
            break;

          default:
            include "not-found.php";
            break;
        }
      }
      ?>
    </div>
    <!-- /.content-wrapper -->
    <?php include '../../struktur/footer.php'; ?>
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <?php include '../../struktur/jquery.php'; ?>
</body>
</html>
