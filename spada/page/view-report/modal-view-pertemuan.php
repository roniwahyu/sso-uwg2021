<?php
include "../../../../sso-uwg/koneksi/koneksi.php";

function tgl_indo($tanggal)
{
  $bulan = array(
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

$idKode = $_GET['kode_mk'];

$query = mysqli_query($koneksi, "SELECT * FROM tbl_spada_mk JOIN users_nama ON tbl_spada_mk.nidn = users_nama.nidn WHERE kode_mk = '$idKode'");
if($query == false){
  die ("Terjadi Kesalahan : ". mysqli_error($koneksi));
}
while($row = mysqli_fetch_array($query)){
?>


  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header header-sipamas">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" align="center"><font color="white"><h4>
          <b>Mata Kuliah - <?php echo $row["kode_mk"]; ?></b>
        </h4></font></h4>
      </div>

      <div class="modal-body">
        <div class="container-fluid">
          <div class="col-md-12 ml-auto">
            <dl class="dl-horizontal">
              <dt></dt>
              <dd><h4 style="color:#4d9ae3;"><b>Data Dosen</b></h4></dd>
              <dt>NIDN</dt>
              <dd><?php echo $row['nidn']; ?></dd>
              <dt>Nama Dosen</dt>
              <dd><?php echo $row['nama_dosen']; ?></dd>
              <dt>Fakultas / Jurusan</dt>
              <dd><?php echo $row['fakultas'] . " - " . $row['jurusan']; ?></dd>
              <hr>
              <dt></dt>
              <dd><h4 style="color:#4d9ae3;"><b>Data Mata Kuliah</b></h4></dd>
              <dt>Kode MK</dt>
              <dd><?php echo $row['kode_mk']; ?></dd>
              <dt>Nama Mata Kuliah</dt>
              <dd><?php echo $row['nama_mk']; ?></dd>
              <dt>Kode <i>Google Classroom</i></dt>
              <dd><?php echo $row['kode_classroom']; ?> </dd>
              <dt>Fakultas / Jurusan</i></dt>
              <dd><?php echo $row['fak'] . " - " . $row['jur']; ?></dd>
              <hr>
              <dt></dt>
              <dd><h4 style="color:#4d9ae3;"><b>Pertemuan Mata Kuliah</b></h4></dd>
              <dt></dt>
            </dl>
            <table class="table table-condensed table-striped table-striped table-hover" id="tabel-data">
              <thead >
                <tr>
                  <th class="text-center" style="width: 110px">Tanggal</th>
                  <th class="text-center">Materi</th>
                  <th class="text-center" style="width: 250px">Tugas</th>
                  <th class="text-center" style="width: 10px">Mahasiswa </th>
                </tr>
              </thead>
              <?php

              $query1 = mysqli_query($koneksi, "SELECT * FROM tbl_spada_pertemuan WHERE kode_mk = '$idKode' ORDER BY tanggal DESC");
              if($query1 == false){
                die ("Terjadi Kesalahan : ". mysqli_error($koneksi));
              }
              while($row1 = mysqli_fetch_array($query1)){

                ?>
                <tr>
                  <td class="text-center"><?php echo tgl_indo($row1['tanggal']); ?></td>
                  <td><?php echo $row1['materi']; ?></td>
                  <td><?php echo $row1['tugas']; ?></td>
                  <td class="text-center"><?php echo $row1['mhs_turn_in'] ?></td>
                </tr>

                <?php
              }
            }
              ?>

            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
