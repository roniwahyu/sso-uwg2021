<footer class="main-footer" style="background-color: #4d9ae3">
  <div class="container">
    <div class="pull-right hidden-xs footer-custom">
      <b>Version</b> 1.1.0
    </div>
    <div class="footer-custom">
      Copyright &copy; 2019-2020. Powered by <strong><a href="www.widyagama.ac.id" class="footer-sipamas-hover">Pusat IT & PDPT Universitas Widyagama</a></strong>.
    </div>
  </div>
</footer>
