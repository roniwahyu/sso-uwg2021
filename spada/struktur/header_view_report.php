<header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand"><b>MONEV </b>E-LEARNING</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="https://sso.widyagama.org/spada/login/signin.php" target="_blank"><i class="fa fa-sign-in"></i> Sign In</a></li>
          </ul>
        </div>

      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
