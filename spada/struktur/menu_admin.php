<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="../../../../assets/img/favicon.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo strtoupper($_SESSION['username']); ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form di menu -->
    <!-- <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form> -->
    <!-- /.search form -->


    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">

      <li class="header">
        <div class="cutom-datetime">
          <h4>
            <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
          </h4>
        </div>
      </li>

      <!-- <li class="header">MAIN NAVIGATION</li> -->
      <li>
        <a href="index.php?page=rekap-pertemuan-mk">
          <i class="fa fa-file-o"></i> <span>Rekap Pertemuan MK</span>
        </a>
      </li>
      <li>
        <a href="index.php?page=daftar-mk">
          <i class="fa fa-file-o"></i> <span>Daftar MK</span>
        </a>
      </li>
      <li>
        <a href="index.php?page=akun-dosen">
          <i class="fa fa-user"></i> <span>Daftar Akun Dosen</span>
        </a>
      </li>
      <li>
        <a href="index.php?page=panduan">
          <i class="fa fa-book"></i> <span>Panduan</span>
          <small class="label pull-right bg-green">new</small>
        </a>
      </li>
      <li class="bg-success"><a href="../../../landing-page" style="background-color: rgb(0, 166, 90);"><i class="fa fa-star-o" style="color: rgb(255, 255, 255);"></i><span style="color: rgb(255, 255, 255);">SSO Landing Page</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
