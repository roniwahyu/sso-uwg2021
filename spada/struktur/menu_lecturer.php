<?php
include "../../../../sso-uwg/koneksi/koneksi.php";
?>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="../../../../assets/img/favicon.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $_SESSION['username']; ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form di menu -->
    <!-- <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form> -->
    <!-- /.search form -->


    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">

      <li class="header">
        <div class="cutom-datetime">
          <h4>
            <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
          </h4>
        </div>
      </li>

      <!-- <li class="header">MAIN NAVIGATION</li> -->
      <li>
        <a href="index.php?page=profile">
          <i class="fa fa-user"></i> <span>Profile Dosen</span>
        </a>
      </li>
      <li>
        <a href="index.php?page=mata-kuliah">
          <i class="fa fa-file-o"></i> <span>Daftar Mata Kuliah</span>
          <span class="pull-right-container">
            <?php
            $sql = "SELECT COUNT(kode_mk) AS num FROM tbl_spada_mk WHERE nidn = '$_SESSION[username]'";
            $data = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
            $row = mysqli_fetch_array($data);
            ?>
          <small class="label pull-right bg-blue"><?php  echo $row['num']; ?></small>
          </span>
        </a>
      </li>
      <li class="treeview" style="height: auto;">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Panduan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <!-- <small class="label pull-right bg-green">new</small> -->
            </span>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="index.php?page=panduan"><i class="fa fa-circle-o"></i> Pengisian Monev</a></li>
            <li><a href="index.php?page=classroom"><i class="fa fa-circle-o"></i>  Google Classroom</a></li>
          </ul>
      </li>
      <li>
        <a href="index.php?page=kontak">
          <i class="fa fa-phone"></i> <span>Info & Pengaduan</span>
            <small class="label pull-right bg-green">new</small>
        </a>
      </li>
      <li class="bg-success"><a href="../../../landing-page" style="background-color: rgb(0, 166, 90);"><i class="fa fa-star-o" style="color: rgb(255, 255, 255);"></i><span style="color: rgb(255, 255, 255);">SSO Landing Page</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
